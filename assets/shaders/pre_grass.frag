#version 330 core

// uniforms
uniform uint shadingModelId;
uniform sampler2D colorTex;

// out
out vec4 gtex_albedoAndGloss;
out vec4 gtex_specularAndShadingModelId;
out vec4 gtex_emi;
out vec4 gtex_normal;

// attributes
in vec3 varPos;
in vec3 varColor;
in vec2 varTexCoord;

void main()
{
	vec4 texColor = texture(colorTex, varTexCoord);
	if(texColor.a < 0.1) discard;
	
	gtex_emi = vec4(varColor * texColor.xyz, 1.0);

	gtex_specularAndShadingModelId.w = float(shadingModelId) / 255.0;
}
