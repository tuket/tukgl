#version 330 core

//Color de salida
out vec4 outColor;
//Variables Variantes
in vec2 varTexCoord;
//Textura
uniform sampler2D colorTex;
uniform sampler2D depthTex;

// uniforms
uniform float near;
uniform float far;
uniform float focalDistance;
uniform float maxDistanceFactor;


#define MASK_SIZE 25u
const vec2 texIdx[MASK_SIZE] = vec2[](
vec2(-2.0,2.0f), vec2(-1.0,2.0f), vec2(0.0,2.0f), vec2(1.0,2.0f), vec2(2.0,2.0),
vec2(-2.0,1.0f), vec2(-1.0,1.0f), vec2(0.0,1.0f), vec2(1.0,1.0f), vec2(2.0,1.0),
vec2(-2.0,0.0f), vec2(-1.0,0.0f), vec2(0.0,0.0f), vec2(1.0,0.0f), vec2(2.0,0.0),
vec2(-2.0,-1.0f), vec2(-1.0,-1.0f), vec2(0.0,-1.0f), vec2(1.0,-1.0f), vec2(2.0,-1.0),
vec2(-2.0,-2.0f), vec2(-1.0,-2.0f), vec2(0.0,-2.0f), vec2(1.0,-2.0f), vec2(2.0,-2.0));

const float dofMaskFactor = float (1.0/65.0);
const float dofMask[MASK_SIZE] = float[](
1.0*dofMaskFactor, 2.0*dofMaskFactor, 3.0*dofMaskFactor,2.0*dofMaskFactor, 1.0*dofMaskFactor,
2.0*dofMaskFactor, 3.0*dofMaskFactor, 4.0*dofMaskFactor,3.0*dofMaskFactor, 2.0*dofMaskFactor,
3.0*dofMaskFactor, 4.0*dofMaskFactor, 5.0*dofMaskFactor,4.0*dofMaskFactor, 3.0*dofMaskFactor,
2.0*dofMaskFactor, 3.0*dofMaskFactor, 4.0*dofMaskFactor,3.0*dofMaskFactor, 2.0*dofMaskFactor,
1.0*dofMaskFactor, 2.0*dofMaskFactor, 3.0*dofMaskFactor,2.0*dofMaskFactor, 1.0*dofMaskFactor);


void main()
{
	//Código del Shader
	float vdepth = textureLod(depthTex, varTexCoord, 0).x;
	float z = - near * far / ( far + vdepth * ( near - far ));

	vec2 ts = vec2(1.0) / vec2 (textureSize (colorTex, 0));
	float dof = abs(z - focalDistance) * maxDistanceFactor;
	dof = clamp (dof, 0.0, 1.0);
	dof *= dof;
	vec4 color = vec4 (0.0);
	for (uint i = 0u; i < MASK_SIZE; i++)
	{
		vec2 iidx = varTexCoord + ts * texIdx[i]*dof;
		color += texture(colorTex, iidx,0.0) * dofMask[i];
	}
	outColor = color;
	
}