uniform PointLight light;

void computeLightParams()
{
	L = normalize(light.Pl - fragData.pos);
	float d = length(light.Pl - fragData.pos);
	Il = light.Il / (light.c0 + light.c1*d + light.c2*d*d);
	Ia = light.Ia;
}