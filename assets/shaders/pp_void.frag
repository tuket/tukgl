#version 330 core

// outputs
out vec4 outColor;

// varyings
in vec2 varTexCoord;

// uniforms
uniform sampler2D colorTex;

void main()
{
	vec4 color = textureLod(colorTex, varTexCoord, 0);
	outColor = color;
}
