#version 330 core

struct PointLight
{
	vec3 Pl;			// light position
	vec3 Ia, Il;		// ambient intensity // diffuse and spec intesity
	float c0, c1, c2;	// attenuation coefficients (constant, linear and quadratic)
};

struct DirLight
{
	vec3 dir;
	vec3 Ia;
	vec3 Il;
};

struct SpotLight
{
	vec3 Pl;			// light position in camera coords
	vec3 dir;			// direction in camera coords
	vec3 Ia, Il;		// ambient intensity // diffuse and specular intensity
	float c0, c1, c2;	// attenuation by distance (constant, linear and quadratic)
	float El;			// cone angle aperture (half)
	float Ed;			// attenuation by angle ([1, +inf], 1->no attenuation)
};

struct FragmentData
{
	uint shadingModelId;
	vec3 pos;
	vec3 normal;
	vec3 albedo;
	vec3 specularIntensity;
	vec3 emiIntensity;
	float  gloss;
};
FragmentData fragData;

// output
out vec4 outColor;

// SHADING MODELS
const uint PHONG = 0u;
const uint UNLIT = 1u;
const uint DEFAULT = PHONG;

// uniforms
uniform mat4 invProjMat;	// we need this to unproject from depth to pos
uniform sampler2D gtex_albedoAndGloss;
uniform sampler2D gtex_specularAndShadingModelId;
uniform sampler2D gtex_emi;
uniform sampler2D gtex_normal;
uniform sampler2D gtex_depth;

// varyings
in vec3 varPos;
in vec2 varTexCoord;

// FOG
/* TODO
const int FOG_NONE = 0;
const int FOG_LINEAR = 1;
const int FOG_EXPONENTIAL = 2;

uniform vec3 fogColor;
uniform float fogStartDistance;
uniform float fogEndDistance;
uniform float fogDensity;	// only for exponential
uniform int fogType;*/

// LIGHT
uniform vec3 globalAmbientIntensity;

vec3 L;		// unit vector from fragment to the light
vec3 Il;	// intensity of the light
vec3 Ia;	// ambient intensity asociated to the light

/////////////////////////////////////////////////////////////////////
#LIGHT
// the preprocessor will insert here the type of light. Example:
/*
uniform DirLight light;

void computeLightParams()
{
	L = - normalize(light.dir);
	Il = light.Il;
	Ia = light.Ia;
}
*/
/////////////////////////////////////////////////////////////////////

void getFragmentDataFromGBuffer()
{

	float depth = textureLod(gtex_depth, varTexCoord, 0).x;
	if(depth > 0.999999)
	{
		discard;
	}
	vec4 f_albedoAndGloss = textureLod(gtex_albedoAndGloss, varTexCoord, 0);
	vec4 f_specularAndShadingModelId = textureLod(gtex_specularAndShadingModelId, varTexCoord, 0);
	vec3 f_emi = textureLod(gtex_emi, varTexCoord, 0).xyz;
	vec3 f_normal = textureLod(gtex_normal, varTexCoord, 0).xyz;
	

	fragData.shadingModelId = uint(255 * f_specularAndShadingModelId.w);
	vec4 pos4 = invProjMat * vec4(varPos.xy, depth*2 - 1, 1);
	fragData.pos = pos4.xyz / pos4.w;
	fragData.normal = 2.0 * f_normal - 1.0;
	fragData.albedo = f_albedoAndGloss.rgb;
	fragData.specularIntensity = f_specularAndShadingModelId.rgb;
	fragData.emiIntensity = f_emi.rgb;
	fragData.gloss = f_albedoAndGloss.a * 255 * 10;

}

vec3 shade_phong()
{
	vec3 color = vec3(0.0);
	
	vec3 N = fragData.normal;
	//return N;
	vec3 V = - normalize(fragData.pos);
	vec3 R = reflect(-L, N);
	vec3 Kd = fragData.albedo;
	vec3 Ks = fragData.specularIntensity;
	float n = fragData.gloss;
	float NdotL = dot(N, L);
	NdotL = clamp(NdotL, 0, 1);
	
	vec3 ambient = Kd * Ia;
	color += ambient;
	
	vec3 diffuse = Kd * Il * NdotL;
	color += diffuse;
	
	float specFactor = max(dot(R, V), 0.00001);
	specFactor = pow(specFactor, n);
	vec3 specular = Ks * Il * specFactor;
	color += specular;
	
	return color;
}

vec3 shade_unlit()
{
	return fragData.emiIntensity;
}

vec3 shade_cartoon()
{
	vec3 color = vec3(0);
	
	vec3 N = fragData.normal;
	vec3 V = - normalize(fragData.pos);
	vec3 R = reflect(-L, N);
	vec3 Kd = fragData.albedo;
	vec3 Ks = fragData.specularIntensity;
	float n = fragData.gloss;
	float NdotL = dot(N, L);
	NdotL = clamp(NdotL, 0, 1);
	
	//vec3 ambient = Kd * Ia;
	//color += ambient;
	
	
	if(NdotL < 0.0)
	{
		vec3 diffuse = Kd * Il * 0.05;
		color += diffuse;
	}
	else if(NdotL < 0.4)
	{
		vec3 diffuse = Kd * Il * 0.2;
		color += diffuse;
	}
	else if(NdotL < 0.7)
	{
		vec3 diffuse = Kd * Il * 0.6;
		color += diffuse;
	}
	else
	{
		vec3 diffuse = Kd * Il * 0.99;
		color += diffuse;
	}
	
	float specFactor = max(dot(R, V), 0.00001);
	specFactor = pow(specFactor, n);
	vec3 specular = vec3(0);
	color += specular;
	
	return color;
}

vec3 shade()
{
	
	switch(fragData.shadingModelId)
	{
		case 0u:
			return shade_phong();
		case 1u:
			return shade_unlit();
		case 2u:
			return shade_cartoon();
			
		default:
			return shade_unlit();
	}
	
}

void main()
{
	getFragmentDataFromGBuffer();
	computeLightParams();
	Il = clamp(Il, 0, 1);
	outColor = vec4(shade(), 1.0);
}