#version 330 core

in vec3 inPos;
out vec3 varPos;
out vec2 varTexCoord;

void main()
{
	varTexCoord = inPos.xy*0.5 + vec2(0.5);
	varPos = inPos;
	gl_Position = vec4(inPos, 1);
}
