#version 330 core

in vec3 inPos;
in vec3 inNormal;

out vec3 vertPos;
out vec3 vertNormal;

void main()
{
	vertNormal = inNormal;

	vertPos = inPos;
	gl_Position = vec4(inPos, 1.0);
}
