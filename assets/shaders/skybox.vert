#version 330 core
in vec3 pos;
out vec3 varTexCoord;

uniform mat4 viewProj;

void main()
{
    gl_Position =   viewProj * (vec4(10.0*pos, 1.0));  
    varTexCoord = vec3(pos.x, -pos.y, pos.z);
} 