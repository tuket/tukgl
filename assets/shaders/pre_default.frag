#version 330 core

// uniforms
uniform uint shadingModelId;
uniform sampler2D colorTex;
uniform sampler2D specularTex;
uniform sampler2D emiTex;
uniform sampler2D normalTex;
uniform float gloss;

// out
out vec4 gtex_albedoAndGloss;
out vec4 gtex_specularAndShadingModelId;
out vec4 gtex_emi;
out vec4 gtex_normal;

// attributes
in vec3 varPos;
in vec3 varNormal;
in vec3 varTangent;
in vec2 varTexCoord;

void main()
{
	vec3 N = varNormal;

	vec4 col = texture(colorTex, varTexCoord);
	
	gtex_albedoAndGloss.rgb = col.rgb;
	gtex_albedoAndGloss.a = gloss / 256.0;		// gloss

	gtex_specularAndShadingModelId.rgb = texture(specularTex, varTexCoord).rgb;
	gtex_specularAndShadingModelId.w = float(shadingModelId) / 255.0;

	gtex_normal.xyz = N / 2 + vec3(0.5);
	
	if(shadingModelId == 1u)	// (unlit)
	{
		gtex_emi = texture(colorTex, varTexCoord);
	}
	else
	{
		gtex_albedoAndGloss.rgb = texture(emiTex, varTexCoord).rgb;
	}
	
}
