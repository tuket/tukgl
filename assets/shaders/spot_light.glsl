uniform SpotLight light;

void computeLightParams()
{
	L = normalize(light.Pl - fragData.pos);
	Ia = light.Ia;
	
	// check if is in the cone
	float cosAngle = dot(light.dir, -L);
	float cosEl = cos(light.El);
	if(cosAngle < cosEl)
	{
		Il = vec3(0.0);
	}
	else
	{
		Il = light.Il;
		// attenuation by distance
		float d = length(light.Pl - fragData.pos);
		Il /= (light.c0 + light.c1*d + light.c2*d*d);
		// attenuetion by angle
		Il *= pow((cosAngle - cosEl) / (1 - cosEl), light.Ed);
	}
}