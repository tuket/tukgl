#version 330 core

// uniforms
uniform uint shadingModelId;
//uniform vec3 baseColor;

// out
out vec4 gtex_albedoAndGloss;
out vec4 gtex_specularAndShadingModelId;
out vec4 gtex_emi;
out vec4 gtex_normal;

// attributes
in vec3 varPos;
in vec3 varNormal;
in vec3 varColor;

void main()
{
	vec3 N = varNormal;
	
	gtex_emi = vec4(varColor, 1.0);

	gtex_specularAndShadingModelId.w = float(shadingModelId) / 255.0;

	gtex_normal.xyz = N / 2 + vec3(0.5);
}
