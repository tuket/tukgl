uniform DirLight light;

void computeLightParams()
{
	L = - normalize(light.dir);
	Il = light.Il;
	Ia = light.Ia;
}