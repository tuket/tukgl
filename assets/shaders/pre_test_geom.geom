#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=256) out;

uniform mat4 modelViewProj;
uniform mat4 normal;
uniform float time;

in vec3 vertPos[];
in vec3 vertNormal[];

out vec3 varPos;
out vec3 varNormal;
out vec3 varColor;

// obtenido de: https://gist.github.com/patriciogonzalezvivo/670c22f3966e662d2f83
///////////////////////////////////////////////////////////////////////////////////
float mod289(float x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 mod289(vec4 x){return x - floor(x * (1.0 / 289.0)) * 289.0;}
vec4 perm(vec4 x){return mod289(((x * 34.0) + 1.0) * x);}

float noise(vec3 p){
    vec3 a = floor(p);
    vec3 d = p - a;
    d = d * d * (3.0 - 2.0 * d);

    vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
    vec4 k1 = perm(b.xyxy);
    vec4 k2 = perm(k1.xyxy + b.zzww);

    vec4 c = k2 + a.zzzz;
    vec4 k3 = perm(c);
    vec4 k4 = perm(c + 1.0);

    vec4 o1 = fract(k3 * (1.0 / 41.0));
    vec4 o2 = fract(k4 * (1.0 / 41.0));

    vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
    vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

    return o4.y * d.y + o4.x * (1.0 - d.y);
}

float rand(float n){return fract(sin(n) * 43758.5453123);}
//////////////////////////////////////////////////////////////////////////////////////

vec3 getNormal()
{
   vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
   vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
   return normalize(cross(a, b));
}

vec3 getUnifBaryCoords(float r1, float r2)
{
	float x = r1;
	float y = r2;
	if(x + y > 1)
	{
		x = 1-x;
		y = 1-y;
	}
	float z = 1 - x - y;
	//return vec3(1.0/3.0);
	return vec3(x, y, z);
	return vec3(1-sqrt(r1), sqrt(r1)*(1-sqrt(r2)), sqrt(r1)*r2);
}

const float PI = 3.14159;
const int numLeafs = 20;
const float leafHeight = 1.0;

void main()
{
	vec3 triNormal = getNormal();
	/*for(int i=0; i<3; i++)
	{
		vec3 pos = gl_in[i].gl_Position.xyz;
		gl_Position = modelViewProj * vec4(pos, 1.0);
		varPos = pos;
		varNormal = vec3(normal * vec4(vertNormal[i], 1.0));
		varColor = vec3(0.8, 0.5, 0.1);
		EmitVertex();
	}
	EndPrimitive();*/
	
	vec3 p1 = gl_in[0].gl_Position.xyz;
	vec3 p2 = gl_in[1].gl_Position.xyz;
	vec3 p3 = gl_in[2].gl_Position.xyz;
	
	vec3 center = (1.0/3.0) * p1 +
				  (1.0/3.0) * p2 +
				  (1.0/3.0) * p3;
				  
	
	float avgToCenter = (distance(center, p1) + distance(center, p2), + distance(center, p3)) / 3.0;
	float leafWidth = avgToCenter * 0.3;
	
	for(int i=0; i<numLeafs; i++)
	{
		vec3 baryCoords = getUnifBaryCoords(rand(i/float(numLeafs)),
		                                    rand(i/float(numLeafs)+0.5));
		//baryCoords = vec3(1.0/3.0, 1.0/3.0, 1.0/3.0);
		vec3 root = mat3(p1, p2, p3) * baryCoords;
		
		float triRot = noise(root) * 2.0 * PI;
		vec2 v1p = leafWidth * vec2(sin(triRot), cos(triRot));
		
		vec3 N = triNormal;
		vec3 bx = normalize(p2 - p1);
		vec3 by = cross(N, bx);
		mat3 basis = mat3(bx, by, N);
		
		float tipDisp = sin(time) * leafWidth;
		vec3 v1 = basis * vec3(v1p, 0);
		vec3 v2 = basis * vec3(-v1p, 0);
		vec3 v3 = -N * leafHeight + basis * (tipDisp * vec3(sin(triRot), cos(triRot), 0.0));
		
		gl_Position = modelViewProj * vec4(root + v1, 1.0);
		varColor = vec3(0.3, 0.5, 0.1);
		EmitVertex();
		
		gl_Position = modelViewProj * vec4(root + v2, 1.0);
		varColor = vec3(0.3, 0.5, 0.1);
		EmitVertex();
		
		gl_Position = modelViewProj * vec4(root + v3, 1.0);
		varColor = vec3(0.5, 0.99, 0.4);
		EmitVertex();
		
		EndPrimitive();
	}
	
}