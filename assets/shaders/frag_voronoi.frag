#version 330

// uniforms
uniform int width;
uniform int height;
uniform float exp;

// out
out vec4 outColor;

// varyings
in vec2 varTexCoord;

#define  HASHSCALE3 vec3(443.897, 441.423, 437.195)

vec2 hash22(vec2 p)
{
	vec3 p3 = fract(vec3(p.xyx) * HASHSCALE3);
    p3 += dot(p3.zxy, p3.yzx+19.19);
    return fract(vec2((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y));
}

void main()
{
	float u = varTexCoord.x * width;
	float v = varTexCoord.y * height;
	
	float x = floor(u);
	float y = floor(v);
	
	int ix = int(x);
	int iy = int(y);
	
	float cx = u - x;
	float cy = v - y;
	vec2 cc = vec2(cx, cy);
	
	// checker board
	//float col = float((ix + iy) % 2);
	//outColor = vec4(vec3(col), 1);
	
	int left = (ix-1+width) % width;
	int right = (ix+1) % width;
	int bot = (iy-1+height) % height;
	int top = (iy+1) % height;
	
	vec2 midP = hash22(vec2(ix, iy));
	vec2 leftP = hash22(vec2(left, iy)) + vec2(-1, 0);
	vec2 rightP = hash22(vec2(right, iy)) + vec2(+1, 0);
	vec2 botP = hash22(vec2(ix, bot)) + vec2(0, -1);
	vec2 topP = hash22(vec2(ix, top)) + vec2(0, +1);
	vec2 botLeftP = hash22(vec2(left, bot)) + vec2(-1, -1);
	vec2 botRightP = hash22(vec2(right, bot)) + vec2(+1, -1);
	vec2 topLeftP = hash22(vec2(left, top)) + vec2(-1, +1);
	vec2 topRightP = hash22(vec2(right, top)) + vec2(+1, +1);
	
	float md = distance(cc, midP);
	md = min(md, distance(cc, leftP));
	md = min(md, distance(cc, rightP));
	md = min(md, distance(cc, botP));
	md = min(md, distance(cc, topP));
	md = min(md, distance(cc, botLeftP));
	md = min(md, distance(cc, botRightP));
	md = min(md, distance(cc, topLeftP));
	md = min(md, distance(cc, topRightP));
	
	md /= 1;
	md = pow(md, 4);
	
	outColor = vec4(vec3(md), 1);
	//outColor = vec4(cc, 0, 1);
	
}
