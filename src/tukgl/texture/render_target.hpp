#ifndef RENDER_TARGET_HPP
#define RENDER_TARGET_HPP

#include <vector>
#include "texture.hpp"

class RenderTarget
{

public:

	RenderTarget(unsigned numTextures = 0,
		unsigned w = 0, unsigned h = 0,
		TextureType texType = TextureType::RGBA8,
		bool depthTex = false);

	RenderTarget(unsigned numTextures,
		unsigned w, unsigned h,
		TextureType* textureTypes,
		bool depthTex = false);

	TextureId getTexture(unsigned slot)const;
	TextureId getDepthTexture()const;

	void resize(unsigned w, unsigned h);

	unsigned getNumTextures()const;

	void bind();

	unsigned getWidth()const { return width; }
	unsigned getHeight()const { return height; }

	unsigned getFbo()const { return fbo; }

	static const unsigned MAX_NUM_TEXTURES = 16;

private:

	std::vector<TextureType> textureTypes;
	std::vector<TextureId> textures;
	TextureId depthTexture;
	unsigned fbo;

	unsigned width, height;

};

#endif