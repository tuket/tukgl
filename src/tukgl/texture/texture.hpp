#ifndef TEXTURE_HPP
#define TEXTURE_HPP

typedef int TextureId;

enum class TextureType
{
	RGB8,
	RGBA8,
	R8,
	R16,
	DEPTH,

	UNKNOWN
};

TextureId loadTextureFromFile(const char* fileName);

TextureId createEmptyTexture(
	unsigned w, unsigned h,
	TextureType texType = TextureType::RGBA8);

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType texType = TextureType::RGBA8
);

void createEmptyTextures(
	TextureId* output, unsigned numTextures,
	unsigned w, unsigned h,
	TextureType* texTypes);

TextureId createEmptyDepthTexture(
	unsigned w, unsigned h,
	TextureType texType = TextureType::DEPTH);

void realeaseTexture(TextureId texId);

unsigned toGlInternalFormat(TextureType texType);

void saveTexture(const char* fileName, TextureId texId);

#endif