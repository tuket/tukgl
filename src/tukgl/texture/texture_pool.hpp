#ifndef TEXTURE_POOL_HPP
#define TEXTURE_POOL_HPP

#include "../util/singleton.hpp"
#include <string>
#include <unordered_map>

typedef int TextureId;

class TexturePool : public Singleton<TexturePool>
{

public:

	TextureId getTexture(const char* fileName);
	void releaseTexture(TextureId texId);

private:

	std::unordered_map<std::string, TextureId> nameToId;
	std::unordered_map<TextureId, std::string> idToName;
	std::unordered_map<TextureId, unsigned> idToCount;

private:
	friend class Singleton<TexturePool>;
	TexturePool() {}

};

#endif