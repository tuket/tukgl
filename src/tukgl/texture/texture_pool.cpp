#include "texture_pool.hpp"

#include "texture.hpp"

using namespace std;

TextureId TexturePool::getTexture(const char* fileName)
{
	string fn(fileName);
	TextureId id;
	auto it = nameToId.find(fn);
	if (it != nameToId.end())
	{
		id = it->second;
	}
	else
	{
		id = nameToId[fn] = loadTextureFromFile(fileName);
		idToName[id] = fn;
		idToCount[id] = 0;
	}
	idToCount[id]++;
	return id;
}

void TexturePool::releaseTexture(TextureId texId)
{
	auto it = idToCount.find(texId);
	if (it->second <= 1)
	{
		auto nameIt = idToName.find(texId);
		string name = nameIt->second;
		idToName.erase(nameIt);
		nameToId.erase(name);
		idToCount.erase(texId);
	}
	else
	{
		it->second--;
	}
}