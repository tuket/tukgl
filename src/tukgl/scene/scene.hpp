#ifndef SCENE_HPP
#define SCENE_HPP

#include <set>
#include "scene_node.hpp"
#include "game_object.hpp"
#include "go_light.hpp"
#include "go_camera.hpp"
#include "go_mesh.hpp"

class Scene
{
public:

	Scene() : root(nullptr), activeCamera(nullptr), skyboxEnabled(false) {}

	void notifyGameObjectAdded(GameObject* gameObject);
	void notifyGameObjectRemoved(GameObject* gameObject);
	void notifySceneNodeAttached(SceneNode* node);
	void notifySceneNodeUnattached(SceneNode* node);

	SceneNode* getRoot() { return root; }
	void setRoot(SceneNode* node);

	const std::set<GOPointLight*>& getPointLights()const { return pointLights; }
	const std::set<GODirLight*>& getDirLights()const { return  dirLights; }
	const std::set<GOSpotLight*>& getSpotLights()const { return spotLights; }

	GOCamera* getActiveCamera() { return activeCamera; }
	void setActiveCamera(GOCamera* camera) { activeCamera = camera; }

	const std::set<GOMesh*>& getMeshes()const { return meshes; }

	bool isSkyboxEnabled()const { return skyboxEnabled; }
	void setSkyboxEnabled(bool yes) { skyboxEnabled = yes; }

	unsigned getSkyboxCubeMap()const { return skyboxCubeMap; }
	void setSkyboxCubeMap(unsigned texId) { skyboxCubeMap = texId; }

private:

	SceneNode* root;

	// lights
	std::set<GOPointLight*> pointLights;
	std::set<GODirLight*> dirLights;
	std::set<GOSpotLight*> spotLights;

	std::set<GOCamera*> cameras;
	GOCamera* activeCamera;

	std::set<GOMesh*> meshes;

	bool skyboxEnabled;
	unsigned skyboxCubeMap;

};

#endif