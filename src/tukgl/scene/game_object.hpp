#ifndef GAME_OBJECT_HPP
#define GAME_OBJECT_HPP

class SceneNode;

class GameObject
{

	friend class SceneNode;

protected:

	GameObject(SceneNode* sceneNode = nullptr);

public:

	SceneNode* getSceneNode() { return sceneNode; }
	const SceneNode* getSceneNode()const { return sceneNode; }
	void setSceneNode(SceneNode* sceneNode);

	virtual ~GameObject() = 0;

	bool isEnabled()const { return enabled; }
	void enable(bool yes = true) { enabled = yes; }

private:

	SceneNode* sceneNode;
	bool enabled;

};

inline GameObject::~GameObject(){}

#endif
