#include "go_camera.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "scene_node.hpp"
#include "../util/auxiliar.hpp"

GOCamera::GOCamera(SceneNode* sceneNode,
	float fovY, float aspectRatio,
	float nearPlane, float farPlane)
	:
	GameObject(sceneNode),
	fovY(fovY),
	aspectRatio(aspectRatio),
	nearPlane(nearPlane),
	farPlane(farPlane)
{}

float GOCamera::getNear()const
{
	return nearPlane;
}

void GOCamera::setNear(float nearPlane)
{
	this->nearPlane = nearPlane;
}

float GOCamera::getFar()const
{
	return farPlane;
}

void GOCamera::setFar(float farPlane)
{
	this->farPlane = farPlane;
}

float GOCamera::getFovY()const
{
	return fovY;
}

void GOCamera::setFovY(float fovY)
{
	this->fovY = fovY;
}

float GOCamera::getAspectRatio()const
{
	return aspectRatio;
}

void GOCamera::setAspectRatio(float aspectRatio)
{
	this->aspectRatio = aspectRatio;
}

glm::mat4 GOCamera::getViewMat()const
{
	const SceneNode * sceneNode = getSceneNode();
	if (sceneNode)
	{
		return glm::inverse(sceneNode->getModelMat());
		//glm::scale(glm::vec3(1.f, 1.f, -1.f)) *
	}
	assert(false);
	return glm::mat4();
}

glm::mat4 GOCamera::getProjMat()const
{
	return glm::perspective(toRad(fovY), aspectRatio, nearPlane, farPlane);
}