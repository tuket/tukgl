#ifndef GO_MESH_HPP
#define GO_MESH_HPP

#include "game_object.hpp"

#include "../render/material.hpp"

typedef unsigned MeshId;

class GOMesh : public GameObject
{
public:

	GOMesh(SceneNode* sceneNode = nullptr);

	MeshId getMeshId()const { return meshId; }
	const Material& getMaterial()const { return material; }
	Material& getMaterial() { return material; }

	void setMeshFromFileName(const char* fileName);
	void setMeshFromCustomName(const char* name);
	void setMeshFromId(MeshId meshId);

	void setMaterial(Material material) { this->material = material; }

	~GOMesh() {}


private:

	MeshId meshId;
	Material material;

public:
	// STATIC
	static GOMesh* createPlane(
		float width=1, float height=1,
		unsigned resW=1, unsigned resH=1,
		float texTilingW=1, float texTilingH=1);

};

#endif