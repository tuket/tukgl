#ifndef SCENE_NODE_HPP
#define SCENE_NODE_HPP

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <set>

class GameObject;
class Scene;

class SceneNode
{

	friend class GameObject;

public:

	SceneNode() : scene(nullptr), parent(nullptr), gameObject(nullptr){}

	std::set<SceneNode*>& getChildern() { return children; }
	void attachChild(SceneNode* node);
	void unattachFromParent();

	GameObject* getGameObject();
	void setGameObject(GameObject* gameObject);
	void unsetGameObject();

	glm::mat4 getLocalModelMat()const;
	glm::mat4 getModelMat()const;

	glm::vec3 getPosition()const;
	glm::vec3 getWorldPosition()const;
	void setPosition(const glm::vec3& position) { this->position = position; }

	glm::quat getRotation()const;
	glm::quat getWorldRotation()const;
	void setRotation(const glm::quat &rotation) { this->rotation = rotation; }

	glm::vec3 getForward()const;
	glm::vec3 getRight()const;
	glm::vec3 getUp()const;

	void moveSelf(const glm::vec3& v);
	void moveLocal(const glm::vec3& v);

	void rotate(const glm::quat& rot);
	void rotateAroundParent(const glm::quat& rot);


private:

	glm::vec3 position;
	glm::quat rotation;

	Scene* scene;
	SceneNode* parent;
	std::set<SceneNode*> children;

	GameObject* gameObject;

};

#endif