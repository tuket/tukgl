#include "go_mesh.hpp"

#include "../mesh/mesh_pool.hpp"
#include <string>
#include <sstream>
#include "../mesh/plane.hpp"

using namespace std;

GOMesh::GOMesh(SceneNode* sceneNode)
	: GameObject(sceneNode)
{
	material = Material::createBasicUnlit();
	material.setUnifValue("baseColor", glm::vec3(0.8, 0.8, 0.8));
}

void GOMesh::setMeshFromFileName(const char* fileName)
{
	MeshPool& pool = MeshPool::getInstance();
	meshId = pool.cacheMeshRequest(fileName);
}

void GOMesh::setMeshFromCustomName(const char* name)
{
	MeshPool& pool = MeshPool::getInstance();
	meshId = pool.getMeshId(name);
}

void GOMesh::setMeshFromId(MeshId meshId)
{
	this->meshId = meshId;
}


// STATIC

GOMesh* GOMesh::createPlane(
	float width, float height,
	unsigned resW, unsigned resH,
	float texTilingW, float texTilingH)
{
	GOMesh* go = new GOMesh();

	stringstream ss;
	ss << "PLANE_" << width << "_" << height
		<< "_" << resW << "_" << resH;
	string name = ss.str();

	Mesh planeMesh = createPlaneMesh(width, height, resW, resH, texTilingW, texTilingH);
	MeshVAO vao;
	MeshVBOs vbos;
	uploadMesh(vao, vbos, planeMesh);

	MeshPool& meshPool = MeshPool::getInstance();
	meshPool.insertCustomMesh(name.c_str(), vao, vbos, planeMesh.numTriangles);

	freeMeshMemory(planeMesh);

	go->setMeshFromCustomName(name.c_str());
	return go;
}