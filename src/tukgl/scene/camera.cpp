#define GLM_FORCE_RADIANS
#include "camera.hpp"

using namespace glm;

vec3 Camera::getPosition() const
{
	return pos;
}
void Camera::setPosition(const vec3& pos)
{
	this->pos = pos;
}

float Camera::getHeading()const
{
	return heading;
}
void Camera::setHeading(float heading)
{
	this->heading = heading;
}

float Camera::getPitch()const
{
	return pitch;
}

void Camera::setPitch(float pitch)
{
	this->pitch = pitch;
}

float Camera::getNear()const
{
	return nearPlane;
}

void Camera::setNear(float nearPlane)
{
	this->nearPlane = nearPlane;
}

float Camera::getFar()const
{
	return farPlane;
}

void Camera::setFar(float farPlane)
{
	this->farPlane = farPlane;
}

float Camera::getFovY()const
{
	return fovY;
}

void Camera::setFovY(float fovY)
{
	this->fovY = fovY;
}

float Camera::getAspectRatio()const
{
	return aspectRatio;
}

void Camera::setAspectRatio(float aspectRatio)
{
	this->aspectRatio = aspectRatio;
}

vec3 Camera::getForwardVec()const
{
	vec3 forward(0, 0, -1);
	mat3 rot = getLocalRotMat();
	forward = rot * forward;
	return forward;
}

glm::vec3 Camera::getRightVec()const
{
	vec3 forward(1, 0, 0);
	mat3 rot = getLocalRotMat();
	forward = rot * forward;
	return forward;
}

mat4 Camera::getViewMat()const
{

	mat4 rotHeading = rotate(mat4(), -heading, vec3(0, 1, 0));
	mat4 rotPitch = rotate(mat4(), -pitch, vec3(1, 0, 0));
	mat4 rot = rotPitch * rotHeading;

	mat4 trans = translate(mat4(), -pos);

	mat4 viewMat = rot * trans;
	return viewMat;

}

glm::mat4 Camera::getProjMat()const
{
	return glm::perspective(fovY, aspectRatio, nearPlane, farPlane);
}


// private

mat3 Camera::getLocalRotMat()const
{

	mat4 rotHeading = glm::rotate(glm::mat4(), heading, glm::vec3(0, 1, 0));
	mat4 rotPitch = glm::rotate(glm::mat4(), pitch, glm::vec3(1, 0, 0));

	mat4 localRotMat = rotHeading * rotPitch;
	return mat3(localRotMat);
}