#include "game_object.hpp"

#include "scene_node.hpp"
#include "scene.hpp"

GameObject::GameObject(SceneNode* sceneNode)
{
	enabled = true;
	setSceneNode(sceneNode);
}

void GameObject::setSceneNode(SceneNode* sceneNode)
{
	this->sceneNode = sceneNode;
	if (sceneNode != nullptr)
	{
		sceneNode->gameObject = this;

		if (sceneNode->scene != nullptr)
		{
			sceneNode->scene->notifyGameObjectAdded(this);
		}
	}
}
