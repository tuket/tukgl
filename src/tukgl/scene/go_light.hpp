#ifndef GO_LIGHT
#define GO_LIGHT

#include "game_object.hpp"
#include <glm/vec3.hpp>

class GOLight : public GameObject
{
protected:

	GOLight(SceneNode* sceneNode = nullptr) : GameObject(sceneNode) { enabled = true; }

public:

	bool isEnabled() { return enabled; }
	void setEnabled(bool enabled) { this->enabled = enabled; }

	virtual ~GOLight() = 0;

private:

	bool enabled;

};
inline GOLight::~GOLight() {}

class GOPointLight : public GOLight
{

public:

	GOPointLight(SceneNode* sceneNode=nullptr) : GOLight(sceneNode) {}

	glm::vec3 getAmbientIntensity()const { return Ia; }
	void setAmbientIntensity(const glm::vec3& Ia) { this->Ia = Ia; }

	glm::vec3 getDiffuseAndSpecularIntensity()const { return Il; }
	void setDiffuseAndSpecularIntensity(const glm::vec3& Il) { this->Il = Il; }

	float getC0()const { return c0; }
	void setC0(float c0) { this->c0 = c0; }

	float getC1()const { return c1; }
	void setC1(float c1) { this->c1 = c1; }

	float getC2()const { return c2; }
	void setC2(float c2) { this->c2 = c2; }

	~GOPointLight(){}


private:

	glm::vec3 Ia, Il;
	float c0, c1, c2;

};

class GODirLight : public GOLight
{
public:

	GODirLight(SceneNode* sceneNode = nullptr) : GOLight(sceneNode) {}

	glm::vec3 getAmbientIntensity()const { return Ia; }
	void setAmbientIntensity(const glm::vec3& Ia) { this->Ia = Ia; }

	glm::vec3 getDiffuseAndSpecularIntensity()const { return Il; }
	void setDiffuseAndSpecularIntensity(const glm::vec3& Il) { this->Il = Il; }

	glm::vec3 getDirection()const { return dir; }
	void setDirection(const glm::vec3& dir) { this->dir = dir; }

	~GODirLight(){}

private:

	glm::vec3 Ia, Il;
	glm::vec3 dir;

};

class GOSpotLight : public GOLight
{
public:

	GOSpotLight(SceneNode* sceneNode = nullptr) : GOLight(sceneNode) {}

private:

	glm::vec3 Ia, Il;
	float c0, c1, c2;
	float El, Ed;

public:

	glm::vec3 getAmbientIntensity()const { return Ia; }
	void setAmbientIntensity(const glm::vec3& Ia) { this->Ia = Ia; }

	glm::vec3 getDiffuseAndSpecularIntensity()const { return Il; }
	void setDiffuseAndSpecularIntensity(const glm::vec3& Il) { this->Il = Il; }

	float getC0()const { return c0; }
	void setC0(float c0) { this->c0 = c0; }

	float getC1()const { return c1; }
	void setC1(float c1) { this->c1 = c1; }

	float getC2()const { return c2; }
	void setC2(float c2) { this->c2 = c2; }

	float getHalfConeAperture()const { return El; }
	void setHalfConeAperture(float El) { this->El = El; }

	float getAttenuationByAngle()const { return Ed; }
	void setAttenuationByAngle(float Ed) { this->Ed = Ed; }

	~GOSpotLight() {}

};

#endif
