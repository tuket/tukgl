#include "import_assimp_mesh.hpp"

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include <iostream>
#include <vector>

using namespace std;

Mesh importAssimpMesh(const char* fileName)
{

	Mesh mesh;

	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile
	(
		fileName,
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType
	);

	if (!scene)
	{
		std::cerr << "error importing assimp mesh" << endl;
		assert(false);
		//return mesh;
	}

	if (scene->HasMeshes())
	{

		aiMesh* m = scene->mMeshes[0];

		unsigned nv = m->mNumVertices;
		unsigned nt = m->mNumFaces;

		mesh.numVertices = nv;
		mesh.numTriangles = nt;
		
		mesh.positions = new float[3 * nv];
		mesh.normals = new float[3 * nv];
		mesh.tangents = new float[3 * nv];
		mesh.colors = new float[3 * nv];
		mesh.texCoords = new float[2 * nv];
		mesh.triangles = new unsigned[3 * nt];

		memcpy(mesh.positions, m->mVertices, 3 * nv * sizeof(float));

		if (m->HasNormals())
		{
			memcpy(mesh.normals, m->mNormals, 3 * nv * sizeof(float));
		}
		if (m->HasTangentsAndBitangents())
		{
			memcpy(mesh.tangents, m->mTangents, 3 * nv * sizeof(float));
		}
		if (m->HasVertexColors(0))
		{
			memcpy(mesh.colors, m->mColors[0], 3 * nv * sizeof(float));
		}

		if(m->HasTextureCoords(0))
		for (unsigned i = 0; i < nv; i++)
		{
			mesh.texCoords[2 * i + 0] = m->mTextureCoords[0][i].x;
			mesh.texCoords[2 * i + 1] = m->mTextureCoords[0][i].y;
		}

		for (unsigned i = 0; i < nt; i++)
		{
			mesh.triangles[3 * i + 0] = m->mFaces[i].mIndices[0];
			mesh.triangles[3 * i + 1] = m->mFaces[i].mIndices[1];
			mesh.triangles[3 * i + 2] = m->mFaces[i].mIndices[2];
		}

	}

	return mesh;

}