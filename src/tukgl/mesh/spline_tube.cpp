#include "spline_tube.hpp"

#include "../mesh/mesh.hpp"
#include "../util/splines.hpp"
#include <iostream>
#include <glm/gtc/constants.hpp>
#include <glm/geometric.hpp>
#include <glm/mat3x3.hpp>
#include "../util/auxiliar.hpp"
#include <random>

using namespace std;
using namespace glm;

Mesh SplineTube::create(const Catmull& spline, float radius, int numStacks,
	int numSlices, float z, bool isCycle)
{
	Mesh mesh;

	if (!isCycle)
	{
		cout << "SplineTube::create: error isCycle==false not implemented" << endl;
		return mesh;
	}

	vector<vec3> circlePos;
	vector<vec3> circleNormal;
	makeSqCircle(numSlices, circlePos, circleNormal, radius, z);

	unsigned nv = (numSlices+1) * (numStacks+1);
	mesh.numVertices = nv;

	vector<vec3> positions(nv);
	vector<vec3> normals(nv);
	vector<vec3> tangents(nv);

	const float splineLen = spline.curveLength();
	// compute positions, normals and tangents
	for (int i = 0; i < numStacks+1; i++)
	{
		float l = (float)i / numStacks * splineLen;
		vec2 lp, lt;	//< center and tangent of the circle
		spline.get(l, lp, lt);

		transformPositions
		(
			positions.begin() + i * (numSlices+1),
			positions.begin() + (i + 1) * (numSlices+1),
			circlePos.begin(), circlePos.end(),
			vec3(lp.x, 0, -lp.y), vec3(lt.x, 0, -lt.y)
		);
		transformVectors
		(
			normals.begin() + i * (numSlices+1),
			normals.begin() + (i + 1) * (numSlices+1),
			circleNormal.begin(), circleNormal.end(),
			vec3(lt.x, 0, -lt.y)
		);

		for (int j = 0; j < numSlices+1; j++)
		{
			tangents[i * (numSlices+1) + j] = vec3(lt.x, 0, -lt.y);
		}
	}

	// compute colors
	random_device r;
	default_random_engine ren(r());
	uniform_real_distribution<float> uniDist(0, 1);
	vector<vec3> colors(nv);
	for (int i = 0; i < nv; i++)
	{
		vec3 col;
		col.r = uniDist(ren);
		col.g = uniDist(ren);
		col.b = uniDist(ren);
		colors[i] = col;
	}

	// compute texCoords
	float tx = 0;
	vector<vec2> texCoords(nv);
	float L = splineLen / (2 * PI * radius);
	L = floor(L + 0.5);
	for (int i = 0; i < numStacks+1; i++)
	{
		tx = i * L / numStacks;
		for (int j = 0; j < numSlices+1; j++)
		{
			float ty = (float)j / numSlices;
			vec2 texCoord(tx, ty);
			texCoords[j + i * (numSlices+1)] = texCoord;
		}
	}

	mesh.initVertexData(nv,
		&positions[0][0], &normals[0][0], &tangents[0][0],
		&colors[0][0], &texCoords[0][0]);


	// connectivity
	// (2 triangles per quad, 3 verts per trangles)
	unsigned nt = numStacks * numSlices * 2;
	vector<unsigned> triangles(nt * 3);

	for (unsigned i = 0; i < numStacks; i++)
	{
		unsigned curStack = i;
		unsigned nextStack = i + 1;
		for (unsigned j = 0; j < numSlices; j++)
		{
			unsigned curSlice = j;
			unsigned nextSlice = j + 1;
			unsigned triIndex1 = (2 * 3) * (curStack * numSlices + curSlice);
			unsigned triIndex2 = (2 * 3) * (curStack * numSlices + curSlice) + 3;
			unsigned v00 = curSlice + curStack * (numSlices+1);
			unsigned v01 = nextSlice + curStack * (numSlices+1);
			unsigned v10 = curSlice + nextStack * (numSlices+1);
			unsigned v11 = nextSlice + nextStack * (numSlices+1);
			triangles[triIndex1 + 0] = v00;
			triangles[triIndex1 + 1] = v11;
			triangles[triIndex1 + 2] = v10;
			triangles[triIndex2 + 0] = v00;
			triangles[triIndex2 + 1] = v01;
			triangles[triIndex2 + 2] = v11;
		}
	}

	mesh.initTrianglesData(nt, triangles.data());

	return mesh;
}


void SplineTube::makeSqCircle(
	unsigned n,
	vector<vec3>& outPos, vector<vec3>& outNormal,
	float radius,
	float z
)
{
	outPos.resize(n+1);
	outNormal.resize(n+1);

	for (unsigned i = 0; i < n+1; i++)
	{
		float angle = ((float)i / n) * 2 * PI;
		float x = cos(angle);
		float y = sin(angle);
		float d = pow(pow(abs(x), z) + pow(abs(y), z), 1.0f / z);
		vec3 p(x/d, y/d, 0);
		outPos[i] = p * radius;

		float sx = sign(x);
		float sy = sign(y);
		x = abs(x);
		y = abs(y);

		const float EPS = 1e-6;
		vec3 normal;
		if (x < EPS)
		{
			normal = vec3(0, -sy, 0);
		}
		else if (y < EPS)
		{
			normal = vec3(-sx, 0, 0);
		}
		else
		{
			float dxdy = pow(pow(x, z) * pow(y, z), 1.f / z - 1.f);
			float dx = -sx * dxdy * pow(x, z - 1.f);
			float dy = -sy * dxdy * pow(y, z - 1.f);
			normal = vec3(dx, dy, 0);
		}
		outNormal[i] = normalize(normal);
	}

}

void SplineTube::transformPositions(
	vector<vec3>::iterator outBegin,
	vector<vec3>::iterator outEnd,
	vector<vec3>::const_iterator inBegin,
	vector<vec3>::const_iterator inEnd,
	const vec3& centerPos, const vec3& tangent
)
{
	const vec3 up(0, 1, 0); // assuming up!
	const vec3 right = cross(normalize(tangent), up);
	mat3 rotMat(right, up, normalize(-tangent));

	auto outIt = outBegin;
	auto inIt = inBegin;
	for (;outIt != outEnd; ++outIt, ++inIt)
	{
		vec3 pos  = *inIt;
		pos = rotMat * pos;
		pos += centerPos;
		*outIt = pos;
	}
}

void SplineTube::transformVectors(
	vector<vec3>::iterator outBegin,
	vector<vec3>::iterator outEnd,
	vector<vec3>::const_iterator inBegin,
	vector<vec3>::const_iterator inEnd,
	const glm::vec3& tangent
)
{

	const vec3 up(0, 1, 0); // assuming up!
	const vec3 right = cross(tangent, up);
	mat3 rotMat(right, up, -tangent);

	auto outIt = outBegin;
	auto inIt = inBegin;
	for (;outIt != outEnd; ++outIt, ++inIt)
	{
		vec3 v = *inIt;
		v = rotMat * v;
		*outIt = v;
	}

}