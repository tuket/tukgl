#ifndef MESH_POOL_HPP
#define MESH_POOL_HPP

#include "../util/singleton.hpp"
#include "mesh.hpp"
#include <vector>
#include <unordered_map>
#include <string>

typedef unsigned MeshId;

class MeshPool : public Singleton<MeshPool>
{
public:

	MeshId cacheMeshRequest(const char* name);
	void cacheMeshRelease(MeshId id);

	MeshId getMeshId(const char* name);
	unsigned getMeshNumTriangles(MeshId id)const;
	MeshVAO getMeshVAO(MeshId id)const;
	const MeshVBOs& getMeshVBOs(MeshId id)const;

	MeshId insertCustomMesh(const char* name,
		MeshVAO meshVAO, const MeshVBOs& meshVBOs,
		unsigned numTriangles);
	// TODO: remove custom mesh

	//~MeshPool() {}

private:

	MeshId nextId;

	std::vector<unsigned> meshNumTriangles;

	std::vector<MeshVAO> meshVAOArray;
	std::vector<MeshVBOs> meshVBOsArray;
	std::unordered_map<std::string, MeshId> nameToId;

	// index of the refcount bucket, -1 means is custom mesh
	std::vector<int> refCountIndices;
	// the number of reference to the mesh file
	std::vector<unsigned> refCount;
	unsigned nextRefCountIndex;

	// private functions
	MeshId supplyId();

private:
	friend class Singleton<MeshPool>;
	MeshPool();

};

#endif