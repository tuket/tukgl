#ifndef PLANE_HPP
#define PLANE_HPP

#include "mesh.hpp"

const int planeNVertex = 4;

const float planeVertexPos[] = { 
	-1.0f,	-1.0f,	 0.0f, 
	 1.0f,	-1.0f,	 0.0f, 
	-1.0f,	 1.0f,	 0.0f, 
	 1.0f,	 1.0f,	 0.0f, 
 };

void createPlaneSimple(unsigned& VAO, unsigned& VBO);

Mesh createPlaneMesh(
	float width=1, float height=1,
	unsigned resW=1, unsigned resH=1,
	float texTilingW=1, float texTilingH=1
);

#endif