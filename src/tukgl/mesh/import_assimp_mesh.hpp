#ifndef IMPORT_ASSIMP_MESH_HPP
#define IMPORT_ASSIMP_MESH_HPP

#include "mesh.hpp"

Mesh importAssimpMesh(const char* fileName);

#endif