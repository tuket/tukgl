#include "sphere.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <vector>
#include <string.h>

const float PI = glm::pi<float>();

using namespace std;

Mesh createSphere(float r, unsigned xres, unsigned yres)
{
	
	Mesh mesh;

	unsigned nv = 2 * xres + (xres + 1) * (yres - 1);
	unsigned nt = 2 * xres + 2 * xres * (yres - 2);
	mesh.numVertices = nv;
	mesh.numTriangles = nt;

	// POSITIONS
	vector<glm::vec3> positions;
	positions.reserve(nt);
	// north pole
	for (unsigned i = 0; i < xres; i++)
	{
		glm::vec3 pos(0, r, 0);
		positions.push_back(pos);
	}

	// meridians
	for (unsigned y = 1; y < yres; y++)
	{
		for (unsigned x = 0; x < xres; x++)
		{
			float yAngle = (float)x / xres * 2 * PI;
			float py = r - 2 * r * ((float)y / yres);
			float pxp = glm::sqrt(r*r - py*py);
			float px = pxp * glm::cos(yAngle);
			float pz = - pxp * glm::sin(yAngle);
			glm::vec3 pos(px, py, pz);
			positions.push_back(pos);
		}
		{
			float yAngle = 0;
			float py = r - 2 * r * ((float)y / yres);
			float pxp = glm::sqrt(r*r - py*py);
			float px = pxp * glm::cos(yAngle);
			float pz = -pxp * glm::sin(yAngle);
			glm::vec3 pos(px, py, pz);
			positions.push_back(pos);
		}
	}

	// south pole
	for (unsigned i = 0; i < xres; i++)
	{
		glm::vec3 pos(0, -r, 0);
		positions.push_back(pos);
	}

	// NORMALS
	vector<glm::vec3> normals(nv);
	for (unsigned i = 0; i < nv; i++)
	{
		const glm::vec3& pos = positions[i];
		glm::vec3 normal = pos / glm::length(pos);
		normals[i] = normal;
	}

	// TANGENTS
	vector<glm::vec3> tangents;
	tangents.reserve(nv);
	// north pole
	for (unsigned i = 0; i < xres; i++)
	{
		float a = (i + 0.5f) / xres;
		float x = -cos(a);
		float y = 0;
		float z = -sin(a);
		glm::vec3 t(x, y, x);
		tangents.push_back(t);
	}

	// meridians
	for (unsigned i = xres; i < nv - xres; i++)
	{
		const glm::vec3& n = normals[i];
		glm::vec3 t;
		t.y = sqrt(n.x*n.x + n.z*n.z);
		float lambda = sqrt( (1 - t.y*t.y) / (n.x*n.x + n.z*n.z) );
		t.x = lambda * n.x;
		t.z = lambda * n.z;
		tangents.push_back(t);
	}

	// south pole
	for (unsigned i = 0; i < xres; i++)
	{
		glm::vec3 t = -tangents[i];
		tangents.push_back(t);
	}

	// COLORS
	vector<glm::vec3> colors(nv);
	for (unsigned i = 0; i < nv; i++)
	{
		glm::vec3 col = glm::abs(normals[i]);
		colors[i] = col;
	}

	// TEX COORDS
	vector<glm::vec2> texCoords;
	texCoords.reserve(nv);
	// north pole
	for (unsigned i = 0; i < xres; i++)
	{
		float x = (i + 0.5f) / xres;
		float y = 1;
		glm::vec2 texCoord(x, y);
		texCoords.push_back(texCoord);
	}

	// meridians
	for (unsigned y = 1; y < yres; y++)
	{
		for (unsigned x = 0; x < xres; x++)
		{
			float tx = (float)x / xres;
			float ty = 1.f - (float)y / yres;
			glm::vec2 texCoord(tx, ty);
			texCoords.push_back(texCoord);
		}
		{
			float tx = 1.f;
			float ty = 1.f - (float)y / yres;
			glm::vec2 texCoord(tx, ty);
			texCoords.push_back(texCoord);
		}
	}

	// south pole
	for (unsigned i = 0; i < xres; i++)
	{
		float x = (i + 0.5f) / xres;
		float y = 0;
		glm::vec2 texCoord(x, y);
		texCoords.push_back(texCoord);
	}

	// TRIANGLES
	vector<glm::uvec3> triangles;
	triangles.reserve(nt);
	// north pole
	for (unsigned i = 0; i < xres; i++)
	{
		unsigned a = i;
		unsigned b = xres + i;
		unsigned c = xres + i + 1;
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}

	// meridians
	for (unsigned y = 0; y < yres - 2; y++)
	{
		for (unsigned x = 0; x < xres; x++)
		{
			glm::uvec3 tri1;
			{
				unsigned a = xres + y * (xres + 1) + x;
				unsigned b = xres + (y + 1) * (xres + 1) + (x + 1);
				unsigned c = xres + y * (xres + 1) + (x + 1);
				tri1 = glm::uvec3(a, b, c);
			}
			glm::uvec3 tri2;
			{
				unsigned a = xres + y * (xres + 1) + x;
				unsigned b = xres + (y + 1) * (xres + 1) + x;
				unsigned c = xres + (y + 1) * (xres + 1) + (x + 1);
				tri2 = glm::uvec3(a, b, c);
			}
			triangles.push_back(tri1);
			triangles.push_back(tri2);
		}
	}

	// south pole
	const unsigned firstSPVert = nv - 2 * xres - 1;
	for (unsigned i = 0; i < xres; i++)
	{
		unsigned a = firstSPVert + i + 1;
		unsigned b = firstSPVert + i;
		unsigned c = firstSPVert + xres + 1 + i;
		glm::uvec3 tri(a, b, c);
		triangles.push_back(tri);
	}

	mesh.positions = new float[3 * nv];
	mesh.normals = new float[3 * nv];
	mesh.tangents = new float[3 * nv];
	mesh.colors = new float[3 * nv];
	mesh.texCoords = new float[2 * nv];
	mesh.triangles = new unsigned[3 * nt];

	memcpy(mesh.positions, &positions[0], 3 * nv * sizeof(float));
	memcpy(mesh.normals, &normals[0], 3 * nv * sizeof(float));
	memcpy(mesh.tangents, &tangents[0], 3 * nv * sizeof(float));
	memcpy(mesh.colors, &colors[0], 3 * nv * sizeof(float));
	memcpy(mesh.texCoords, &texCoords[0], 2 * nv * sizeof(float));
	memcpy(mesh.triangles, &triangles[0], 3 * nt * sizeof(unsigned));

	return mesh;

}
