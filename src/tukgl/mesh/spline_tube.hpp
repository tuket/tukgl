#ifndef SPLINE_TUBE_HPP
#define SPLINE_TUBE_HPP

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>

struct Mesh;
class Catmull;

class SplineTube
{

public:

	static Mesh create(
		const Catmull& spline, float radius,
		int numStacks = 100, int numSlices = 10,
		float z = 2,	// controls how squared the tube is.
						// 2 is circular, inf is perfect square
		bool isCycle = true);

private:

	static void makeSqCircle(
		unsigned n,
		std::vector<glm::vec3>& outPos, std::vector<glm::vec3>& outNormal,
		float radius,
		float z = 2		// controls how squared the tube is.
						// 2 is circular, inf is perfect square
	);

	static void transformPositions(
		std::vector<glm::vec3>::iterator outBegin,
		std::vector<glm::vec3>::iterator outEnd,
		std::vector<glm::vec3>::const_iterator inBegin,
		std::vector<glm::vec3>::const_iterator inEnd,
		const glm::vec3& centerPos, const glm::vec3& tangent
	);

	static void transformVectors(
		std::vector<glm::vec3>::iterator outBegin,
		std::vector<glm::vec3>::iterator outEnd,
		std::vector<glm::vec3>::const_iterator inBegin,
		std::vector<glm::vec3>::const_iterator inEnd,
		const glm::vec3& tangent
	);
};

#endif