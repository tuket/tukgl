#include "plane.hpp"

#include <glad/glad.h>
#include "../shader/shader.hpp"
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

using namespace std;
using namespace glm;

void createPlaneSimple(unsigned& VAO, unsigned& VBO)
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex * sizeof(float) * 3,
		planeVertexPos, GL_STATIC_DRAW);
	glVertexAttribPointer((int)PrePassShader::EAttribLocations::pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray((int)PrePassShader::EAttribLocations::pos);	// OPTIM: necessary?
}

Mesh createPlaneMesh(
	float width, float height,
	unsigned resW, unsigned resH,
	float texTilingW, float texTilingH)
{
	Mesh mesh;

	unsigned W = resW + 1;
	unsigned H = resH + 1;
	unsigned nv = W * H;
	unsigned nt = 2 * resW * resH;

	// POSITIONS
	float minX = - width / 2.f;
	float minZ = - height / 2.f;
	float dx = width / resW;
	float dz = height / resH;
	vector<vec3> positions(nv);
	for (unsigned iz = 0; iz < H; iz++)
	{
		float z = minZ + iz * dz;
		for (unsigned ix = 0; ix < W; ix++)
		{
			unsigned i = ix + W * iz;
			float x = minX + ix * dx;
			positions[i] = vec3(x, 0, z);
		}
	}

	//NORMALS
	vector<vec3> normals(nv);
	for (unsigned i = 0; i < nv; i++) normals[i] = vec3(0, 1, 0);

	// TANGENTS
	vector<vec3> tangents(nv);
	for (unsigned i = 0; i < nv; i++) tangents[i] = vec3(1, 0, 0);

	// COLORS
	vector<vec3> colors(nv);
	for (unsigned iz = 0; iz < H; iz++)
	{
		float g = float(iz) / resH;
		for (unsigned ix = 0; ix < W; ix++)
		{
			unsigned i = ix + W * iz;
			float r = float(ix) / resW;
			colors[i] = vec3(r, g, 0);
		}
	}

	// TEX COORDS
	vector<vec2> texCoords(nv);
	for (unsigned iz = 0; iz < H; iz++)
	{
		float v = float(iz) / resH;
		for (unsigned ix = 0; ix < W; ix++)
		{
			unsigned i = ix + W * iz;
			float u = float(ix) / resW;
			texCoords[i] = vec2(u * texTilingW, v * texTilingH);
		}
	}

	// TRIANGLES
	vector<uvec3> triangles;
	triangles.reserve(nt);
	for (unsigned iz = 0; iz < resH; iz++)
	{
		for (unsigned ix = 0; ix < resW; ix++)
		{
			unsigned i0 = ix + iz * W;
			unsigned i1 = (ix + 1) + iz * W;
			unsigned i2 = ix + (iz + 1) * W;
			unsigned i3 = (ix + 1) + (iz + 1) * W;
			triangles.push_back(uvec3(i0, i3, i1));
			triangles.push_back(uvec3(i0, i2, i3));
		}
	}

	mesh.initVertexData(nv,
		&positions[0][0],
		&normals[0][0],
		&tangents[0][0],
		&colors[0][0],
		&texCoords[0][0]);

	mesh.initTrianglesData(nt,
		&triangles[0][0]);

	return mesh;
}