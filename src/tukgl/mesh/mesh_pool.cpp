#include "mesh_pool.hpp"

#include "mesh.hpp"
#include "import_assimp_mesh.hpp"
#include <iostream>

using namespace std;

MeshId MeshPool::getMeshId(const char* name)
{
	auto it = nameToId.find(name);
	return it->second;
}

MeshId MeshPool::cacheMeshRequest(const char* name)
{
	auto it = nameToId.find(name);
	if (it == nameToId.end() || refCount[it->second] == 0)
	{
		Mesh mesh = importAssimpMesh(name);
		MeshId id;
		if (it == nameToId.end())
		{
			id = supplyId();
			nameToId[name] = id;
			it = nameToId.find(name);
		}
		else
		{
			id = it->second;
		}
		
		MeshVAO vao;
		MeshVBOs vbos;
		uploadMesh(vao, vbos, mesh);
		if (meshVAOArray.size() == id)
		{
			meshNumTriangles.push_back(mesh.numTriangles);
			meshVAOArray.push_back(vao);
			meshVBOsArray.push_back(vbos);
			nameToId[name] = id;
			refCountIndices.push_back(refCount.size());
			refCount.push_back(0);
		}
		else
		{
			meshNumTriangles[id] = mesh.numTriangles;
			meshVAOArray[id] = vao;
			meshVBOsArray[id] = vbos;
		}
		freeMeshMemory(mesh);
	}
	refCount[ refCountIndices[it->second] ] ++;
	return it->second;
}

void MeshPool::cacheMeshRelease(MeshId id)
{
	int refCountIndex = refCountIndices[id];
	if (refCountIndex < 0)
	{
		cout << "cacheMeshRelease: warning attempt to apply to a custom mesh" << endl;
	}
	else
	{
		refCount[refCountIndex]--;
		if (refCount[refCountIndex] == 0)
		{
			freeMeshVAO(meshVAOArray[id]);
			freeMeshVBOs(meshVBOsArray[id]);
		}
	}
	
}

unsigned MeshPool::getMeshNumTriangles(MeshId id)const
{
	return meshNumTriangles[id];
}

MeshVAO MeshPool::getMeshVAO(MeshId id)const
{
	return meshVAOArray[id];
}

const MeshVBOs& MeshPool::getMeshVBOs(MeshId id)const
{
	return meshVBOsArray[id];
}

MeshId MeshPool::insertCustomMesh(const char* name,
	MeshVAO meshVAO, const MeshVBOs& meshVBOs,
	unsigned numTriangles)
{
	MeshId meshId = supplyId();
	nameToId[name] = meshId;
	meshVAOArray.push_back(meshVAO);
	meshVBOsArray.push_back(meshVBOs);
	meshNumTriangles.push_back(numTriangles);
	refCountIndices.push_back(-1);
	return meshId;
}

MeshId MeshPool::supplyId()
{
	MeshId id = nextId;
	nextId++;
	return id;
}

MeshPool::MeshPool() : nextId(0), nextRefCountIndex(0)
{
	
}