#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "mesh.hpp"

Mesh createSphere(float radius, unsigned slices, unsigned stacks);

#endif