#ifndef CYLINDER_HPP
#define CYLINDER_HPP

#include "mesh.hpp"

Mesh createCylinder(float radius, float height, unsigned slices);

#endif