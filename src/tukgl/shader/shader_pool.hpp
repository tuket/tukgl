#ifndef SHADER_POOL_HPP
#define SHADER_POOL_HPP

#include "shader.hpp"
#include "../util/singleton.hpp"
#include <unordered_map>
#include <string>
#include <vector>

typedef unsigned ShaderId;
typedef unsigned PrePassShaderId;

class ShaderPool : public Singleton<ShaderPool>
{
public:

	bool isPrePassShaderRegistered(const char* name)const;

	PrePassShaderId registerPrePassShader(const char* name,
		const char* vertShadName, const char* fragShadName);

	PrePassShaderId registerPrePassShader(const char* name,
		const char* vertShadName, const char* geomShadName, const char* fragShadName);

	PrePassShaderId getPrePassShaderIdFromName(const char* name)const;

	const PrePassShader* getPrePassShader(PrePassShaderId id)const;

	PrePassShaderId getDefaultPrePassShaderId()const;

private:

	ShaderId nextId;

	std::unordered_map<std::string, PrePassShaderId> prePassNameToId;

	std::vector<PrePassShader*> idToShader;

	// functions
	void initPredefinedShaders();

private:
	friend class Singleton<ShaderPool>;
	ShaderPool();

};

#endif