#include "shader_pool.hpp"

#include "predefined_shaders.hpp"
#include <cassert>

using namespace std;

ShaderPool::ShaderPool()
{
	nextId = 0;
	initPredefinedShaders();
}

bool ShaderPool::isPrePassShaderRegistered(const char* name)const
{
	unordered_map<std::string, PrePassShaderId>::const_iterator it;
	it = prePassNameToId.find(name);
	return it != prePassNameToId.end();
}

PrePassShaderId ShaderPool::registerPrePassShader(const char* name,
	const char* vertShadName, const char* fragShadName)
{
	// TODO: clean, reuse entries
	PrePassShaderId id = nextId;
	PrePassShader* prePassShader = new PrePassShader(vertShadName, fragShadName);
	idToShader.push_back(prePassShader);
	prePassNameToId[name] = nextId;
	nextId++;
	return id;
}

PrePassShaderId ShaderPool::registerPrePassShader(const char* name,
	const char* vertShadName, const char* geomShadName, const char* fragShadName)
{
	// TODO: clean, reuse entries
	PrePassShaderId id = nextId;
	PrePassShader* prePassShader = new PrePassShader(vertShadName, geomShadName, fragShadName);
	idToShader.push_back(prePassShader);
	prePassNameToId[name] = nextId;
	nextId++;
	return id;
}

PrePassShaderId ShaderPool::getPrePassShaderIdFromName(const char* name)const
{
	return prePassNameToId.find(name)->second;
}

const PrePassShader* ShaderPool::getPrePassShader(PrePassShaderId id)const
{
	assert(id >= 0 && id < idToShader.size());
	return idToShader[id];
}

PrePassShaderId ShaderPool::getDefaultPrePassShaderId()const
{
	//return getPrePassShaderIdFromName("default");
	return 0;
}

void ShaderPool::initPredefinedShaders()
{

	// DEFAULT
	registerPrePassShader(PreDefShad::defaultShad,
		"shaders/pre_default.vert", "shaders/pre_default.frag");

	// BUMP MAPPING
	registerPrePassShader(PreDefShad::bumpMapping,
		"shaders/pre_default.vert", "shaders/pre_bumpMapping.frag");

	// WIREFRAME
	registerPrePassShader(PreDefShad::wireframe,
		"shaders/pre_wireframe.vert", "shaders/pre_wireframe.frag");

	// SIMPLE UNLIT COLOR
	registerPrePassShader(PreDefShad::simpleUnlitColor,
		"shaders/pre_simple_unlit.vert", "shaders/pre_simple_unlit.frag");

}
