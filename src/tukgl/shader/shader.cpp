#include "shader.hpp"

#include "../util/auxiliar.hpp"

#include <glad/glad.h>
#include <iostream>
#include <string.h>

using namespace std;

static GLuint loadShader(const char *fileName, GLenum type);
static GLuint compileShaderFromSrc(const char* src, GLenum type);

const char* Shader::simpleQuadVSFileName = "shaders/simple_quad.vert";

bool Shader::compile(const char* vertShad, const char* fragShad)
{
	hasGeomShader = false;
	vshader = loadShader(vertShad, GL_VERTEX_SHADER);
	fshader = loadShader(fragShad, GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	return true;
}

bool Shader::compileFromSrc(const char* vsSrc, const char* fsSrc)
{
	hasGeomShader = false;
	vshader = compileShaderFromSrc(vsSrc, GL_VERTEX_SHADER);
	fshader = compileShaderFromSrc(fsSrc, GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	return true;
}

bool Shader::compile(const char* vertShad, const char* geomShad, const char* fragShad)
{
	hasGeomShader = true;
	vshader = loadShader(vertShad, GL_VERTEX_SHADER);
	gshader = loadShader(geomShad, GL_GEOMETRY_SHADER);
	fshader = loadShader(fragShad, GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, gshader);
	glAttachShader(program, fshader);
	return true;
}

bool Shader::compileFromSrc(const char* vsSrc, const char* gsSrc, const char* fsSrc)
{
	hasGeomShader = true;
	vshader = compileShaderFromSrc(vsSrc, GL_VERTEX_SHADER);
	gshader = compileShaderFromSrc(gsSrc, GL_GEOMETRY_SHADER);
	fshader = compileShaderFromSrc(fsSrc, GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vshader);
	glAttachShader(program, gshader);
	glAttachShader(program, fshader);
	return true;
}

bool Shader::link()
{
	glLinkProgram(program);
	int linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetProgramInfoLog(program, logLen, NULL, logString);
		std::cout << "Error: " << logString << std::endl;
		delete[] logString;
		glDeleteProgram(program);
		program = 0;
		assert(false && "error linking shaders");
	}

	// get uniform locations
	int numUnifs;
	glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &numUnifs);
	for (int i = 0; i < numUnifs; i++)
	{
		char name[2048];
		int length;
		int size;
		GLenum type;
		glGetActiveUniform(program, (GLuint)i, 2048, &length, &size, &type, name);
		int loc = glGetUniformLocation(program, name);

		unifNameToLoc[string(name)] = loc;
		unifType[loc] = fromGlUnifType(type);
	}

	return true;
}

void Shader::use()const
{
	glUseProgram(program);
}

int Shader::getUnifLoc(const char* varName)const
{
    int unifLoc = glGetUniformLocation(program, varName);
	return glGetUniformLocation(program, varName);
}

Shader::UnifType Shader::getUnifType(int loc)const
{
	auto it = unifType.find(loc);
	assert(it != unifType.end());
	return it->second;
}
Shader::UnifType Shader::getUnifType(const char* varName)const
{
	auto it = unifNameToLoc.find(varName);
	assert(it != unifNameToLoc.end());
	return getUnifType(it->second);
}

// UPLOAD UNIFS
void Shader::uploadUnifVal(int loc, const UnifData& data)const
{
	UnifType unifType = getUnifType(loc);

	switch (unifType)
	{
	case UnifType::FLOAT:
		uploadUnifVal(loc, data.FLOAT);
		break;
	case UnifType::VEC2:
		uploadUnifVal(loc, glm::vec2(data.VEC[0], data.VEC[1]));
		break;
	case UnifType::VEC3:
		uploadUnifVal(loc, glm::vec3(data.VEC[0], data.VEC[1], data.VEC[2]));
		break;
	case UnifType::VEC4:
		uploadUnifVal(loc, glm::vec4(data.VEC[0], data.VEC[1], data.VEC[2], data.VEC[3]));
		break;
	case UnifType::INT:
		uploadUnifVal(loc, data.INT);
		break;
	case UnifType::UNSIGNED:
		uploadUnifVal(loc, data.UNSIGNED);
		break;
	default:
		assert(false && "type of uniform not implemented");
	}
}

void Shader::uploadUnifVal(int loc, float x)const
{
	assert(getUnifType(loc) == UnifType::FLOAT);
	glUniform1f(loc, x);
}
void Shader::uploadUnifVal(int loc, glm::vec2 v)const
{
	assert(getUnifType(loc) == UnifType::VEC2);
	glUniform2fv(loc, 1, &v[0]);
}
void Shader::uploadUnifVal(int loc, glm::vec3 v)const
{
	assert(getUnifType(loc) == UnifType::VEC3);
	glUniform3fv(loc, 1, &v[0]);
}
void Shader::uploadUnifVal(int loc, glm::vec4 v)const
{
	assert(getUnifType(loc) == UnifType::VEC4);
	glUniform4fv(loc, 1, &v[0]);
}
void Shader::uploadUnifVal(int loc, int x)const
{
	assert(getUnifType(loc) == UnifType::INT);
	glUniform1i(loc, x);
}
void Shader::uploadUnifVal(int loc, unsigned x)const
{
	assert(getUnifType(loc) == UnifType::UNSIGNED);
	glUniform1ui(loc, x);
}

Shader::UnifType Shader::fromGlUnifType(unsigned glUnifType)
{
	static const map<GLenum, UnifType> mapto =
	{	{GL_FLOAT, UnifType::FLOAT},
		{GL_FLOAT_VEC2, UnifType::VEC2}, {GL_FLOAT_VEC3, UnifType::VEC3},{GL_FLOAT_VEC4, UnifType::VEC4},
		{GL_INT, UnifType::INT}, {GL_UNSIGNED_INT, UnifType::UNSIGNED},
		{GL_SAMPLER_2D, UnifType::INT}
	};

	auto it = mapto.find(glUnifType);
	if (it == mapto.end())
	{
		return UnifType::OTHER;
	}

	return it->second;
}

// PostProcessShader

const char* PostProcessShader::colorTexStr = "colorTex";
const char* PostProcessShader::depthTexStr = "depthTex";

PostProcessShader::PostProcessShader(const char* frag)
{
	compile(simpleQuadVSFileName, frag);
	link();
	texLoc = glGetUniformLocation(program, colorTexStr);
	if (texLoc == -1)
	{
		cout << "Warning: PostProcessShader \"" << frag << "\" does not define colorTex" << endl;
	}
	depthTexLoc = glGetUniformLocation(program, depthTexStr);
	if (depthTexLoc == -1)
	{
		cout << "Warning: PostProcessShader \"" << frag << "\" does not define depthTex" << endl;
	}
}

void PostProcessShader::setInputTexture(unsigned tex)
{
	texId = tex;
}

void PostProcessShader::setInputDepthTexture(unsigned tex)
{
	depthTexId = tex;
}

void PostProcessShader::setOutputFBO(unsigned FBO)
{
	outFBO = FBO;
}

void PostProcessShader::use()const
{
	glBindFramebuffer(GL_FRAMEBUFFER, outFBO);

	glUseProgram(program);

	if (texLoc != -1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texId);
		glUniform1i(texLoc, 0);
	}
	if (depthTexLoc != -1)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthTexId);
		glUniform1i(depthTexLoc, 1);
	}
}

// PrePassShader
PrePassShader::PrePassShader(const char* vert, const char* frag)
{
	compile(vert, frag);
	initPrePass();
}

PrePassShader::PrePassShader(const char* vert, const char* geom, const char* frag)
{
	compile(vert, geom, frag);
	initPrePass();
}

void PrePassShader::initPrePass()
{
	glBindAttribLocation(program, (unsigned)EAttribLocations::pos, "inPos");
	glBindAttribLocation(program, (unsigned)EAttribLocations::color, "inColor");
	glBindAttribLocation(program, (unsigned)EAttribLocations::normal, "inNormal");
	glBindAttribLocation(program, (unsigned)EAttribLocations::texCoord, "inTexCoord");
	glBindAttribLocation(program, (unsigned)EAttribLocations::tangent, "inTangent");

	glBindFragDataLocation(program, 0, "gtex_albedoAndGloss");
	glBindFragDataLocation(program, 1, "gtex_specularAndShadingModelId");
	glBindFragDataLocation(program, 2, "gtex_emi");
	glBindFragDataLocation(program, 3, "gtex_normal");

	link();

	unifLocs.colorTex = glGetUniformLocation(program, "colorTex");
	unifLocs.specularTex = glGetUniformLocation(program, "specularTex");
	unifLocs.normalTex = glGetUniformLocation(program, "normalTex");
	unifLocs.emiTex = glGetUniformLocation(program, "emiTex");
	unifLocs.customTex = glGetUniformLocation(program, "customTex");

	unifLocs.model = glGetUniformLocation(program, "model");
	unifLocs.modelView = glGetUniformLocation(program, "modelView");
	unifLocs.modelViewProj = glGetUniformLocation(program, "modelViewProj");
	unifLocs.normal = glGetUniformLocation(program, "normal");

	unifLocs.camPos = glGetUniformLocation(program, "camPos");

	unifLocs.shadingModelId = glGetUniformLocation(program, "shadingModelId");

	unifLocs.gloss = glGetUniformLocation(program, "gloss");

	attribLocs.pos = glGetAttribLocation(program, "inPos");
	attribLocs.color = glGetAttribLocation(program, "inColor");
	attribLocs.normal = glGetAttribLocation(program, "inNormal");
	attribLocs.texCoord = glGetAttribLocation(program, "inTexCoord");
	attribLocs.tangent = glGetAttribLocation(program, "inTangent");
}

GLuint loadShader(const char *fileName, GLenum type)
{
	int fileLen[1];
	const char* source[1] = {loadStringFromFile(fileName, fileLen[0])};

	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, source, fileLen);
	glCompileShader(shader);
	delete[] source[0];
    
    // check errors
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		cout << "FILE NAME: " << fileName << endl;
		cout << "Error: " << logString << endl;
		delete[] logString;
		glDeleteShader(shader);
		assert(true);
		exit(-1);
	}

	return shader;

}

GLuint compileShaderFromSrc(const char* src, GLenum type)
{
	int fileLen[1] = {(int)strlen(src)};
	const char* source[1] = {src};

	GLuint shader;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, source, fileLen);
	glCompileShader(shader);
    
    // check errors
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		//Calculamos una cadena de error
		GLint logLen;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLen);
		char *logString = new char[logLen];
		glGetShaderInfoLog(shader, logLen, NULL, logString);
		cout << "compileShaderFromSrc: " << endl << src << endl;
		cout << "Error: " << logString << endl;
		delete[] logString;
		glDeleteShader(shader);
		exit(-1);
	}

	return shader;
}

// SKYBOX

const char* SkyboxShader::vertShadFileName = "shaders/skybox.vert";
const char* SkyboxShader::fragShadFileName = "shaders/skybox.frag";

SkyboxShader::SkyboxShader()
{
	compile(vertShadFileName, fragShadFileName);
	
	glBindAttribLocation(program, (int)EAattribLocations::pos, "pos");
	
	link();

	posAttribLoc = glGetAttribLocation(program, "pos");
	if (posAttribLoc == -1) cerr << "Error: pos not declared in skybox shader" << endl;

	unifLocs.colorTex = glGetUniformLocation(program, "colorTex");
	if (unifLocs.colorTex == -1) cerr << "warning: colorTex not declared in shader" << endl;

	unifLocs.viewProj = glGetUniformLocation(program, "viewProj");
	if (unifLocs.viewProj == -1) cerr << "warning: viewProj not declared in skybox shader" << endl;

}
