#include "light_shaders.hpp"

#include "../util/auxiliar.hpp"
#include <glad/glad.h>
#include <iostream>

using namespace std;

static const char* inPosStr = "inPos";

static const char* postPassFSFileName = "shaders/post_pass.frag";

// POINT LIGHT
const char* PointLightShader::fileName = "shaders/point_light.glsl";

PointLightShader::PointLightShader()
{
	int len;
	const char* vsSrc = loadStringFromFile(simpleQuadVSFileName, len);
	const char* postPassSrc = loadStringFromFile(postPassFSFileName, len);
	const char* lightSrc = loadStringFromFile(fileName, len);
	string fsSrc(postPassSrc);
	replaceStr(fsSrc, "#LIGHT", lightSrc);
	compileFromSrc(vsSrc, fsSrc.c_str());

	glBindAttribLocation(program, 0, inPosStr);

	link();

	// get attrib locations
	inPosAttribLoc = glGetAttribLocation(program, inPosStr);
	if (inPosAttribLoc == -1) cerr << "Error: inPos is not defined in vertex shader" << endl;

	// get uniform locations
	string plName = "light";
	string unifName;

	unifName = plName + ".Pl";
	unif.Pl = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c0";
	unif.c0 = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c1";
	unif.c1 = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c2";
	unif.c2 = glGetUniformLocation(program, unifName.c_str());

	// gtex locs
	unifName = "gtex_albedoAndGloss";
	gtexUnif.albedoAndGloss = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_specularAndShadingModelId";
	gtexUnif.specularAndShadingModelId = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_emi";
	gtexUnif.emi = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_normal";
	gtexUnif.normal = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_depth";
	gtexUnif.depth = glGetUniformLocation(program, unifName.c_str());

	// invProjMat
	unifName = "invProjMat";
	invProjMatUnif = glGetUniformLocation(program, unifName.c_str());
}

const PointLightUniformLocations& PointLightShader::getUniformLocations()const
{
	return unif;
}

// DIR LIGHT
const char* DirLightShader::fileName = "shaders/dir_light.glsl";

DirLightShader::DirLightShader()
{
	int len;
	const char* vsSrc = loadStringFromFile(simpleQuadVSFileName, len);
	const char* postPassSrc = loadStringFromFile(postPassFSFileName, len);
	const char* lightSrc = loadStringFromFile(fileName, len);
	string fsSrc(postPassSrc);
	replaceStr(fsSrc, "#LIGHT", lightSrc);
	compileFromSrc(vsSrc, fsSrc.c_str());

	glBindAttribLocation(program, 0, inPosStr);

	link();

	// get attrib locations
	inPosAttribLoc = glGetAttribLocation(program, inPosStr);
	if (inPosAttribLoc == -1) cerr << "Error: inPos is not defined in vertex shader" << endl;

	// get uniforms
	string dlName = "light";
	string unifName;

	unifName = dlName + ".dir";
	unif.dir = glGetUniformLocation(program, unifName.c_str());

	unifName = dlName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = dlName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());

	// gtex locs
	unifName = "gtex_albedoAndGloss";
	gtexUnif.albedoAndGloss = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_specularAndShadingModelId";
	gtexUnif.specularAndShadingModelId = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_emi";
	gtexUnif.emi = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_normal";
	gtexUnif.normal = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_depth";
	gtexUnif.depth = glGetUniformLocation(program, unifName.c_str());

	// invProjMat
	unifName = "invProjMat";
	invProjMatUnif = glGetUniformLocation(program, unifName.c_str());

}

const DirLightUniformLocations& DirLightShader::getUniformLocations()const
{
	return unif;
}

// SPOT LIGHT
const char* SpotLightShader::fileName = "shaders/spot_light.glsl";

SpotLightShader::SpotLightShader()
{
	int len;
	const char* vsSrc = loadStringFromFile(simpleQuadVSFileName, len);
	const char* postPassSrc = loadStringFromFile(postPassFSFileName, len);
	const char* lightSrc = loadStringFromFile(fileName, len);
	string fsSrc(postPassSrc);
	replaceStr(fsSrc, "#LIGHT", lightSrc);
	compileFromSrc(vsSrc, fsSrc.c_str());

	glBindAttribLocation(program, 0, inPosStr);

	link();

	// get attrib locations
	inPosAttribLoc = glGetAttribLocation(program, inPosStr);
	if (inPosAttribLoc == -1) cerr << "Error: inPos is not defined in vertex shader" << endl;

	// get uniforms
	string slName = "light";
	string unifName;

	unifName = slName + ".Pl";
	unif.Pl = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".dir";
	unif.dir = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c0";
	unif.c0 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c1";
	unif.c1 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c2";
	unif.c2 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".El";
	unif.El = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Ed";
	unif.Ed = glGetUniformLocation(program, unifName.c_str());

	// gtex locs
	unifName = "gtex_albedoAndGloss";
	gtexUnif.albedoAndGloss = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_specularAndShadingModelId";
	gtexUnif.specularAndShadingModelId = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_emi";
	gtexUnif.emi = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_normal";
	gtexUnif.normal = glGetUniformLocation(program, unifName.c_str());

	unifName = "gtex_depth";
	gtexUnif.depth = glGetUniformLocation(program, unifName.c_str());

	// invProjMat
	unifName = "invProjMat";
	invProjMatUnif = glGetUniformLocation(program, unifName.c_str());
}

const SpotLightUniformLocations& SpotLightShader::getUniformLocations()const
{
	return unif;
}