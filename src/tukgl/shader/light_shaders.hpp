#ifndef LIGHT_SHADERS
#define LIGHT_SHADERS

#include "shader.hpp"

#include "../render/lights.hpp"

struct GBufferUniformLocations
{
	int albedoAndGloss;
	int specularAndShadingModelId;
	int emi;
	int normal;
	int depth;
};

class PointLightShader : public Shader
{
private:
	static const char* fileName;	// piece of code to be inserted in post_pass.frag

public:

	PointLightShader();

	int getInPosAttribLocation()const { return inPosAttribLoc; }

	int getInvProjMatUniform()const { return invProjMatUnif; }
	const PointLightUniformLocations& getUniformLocations()const;
	const GBufferUniformLocations& getGBufferUniformLocations()const { return gtexUnif; }

private:

	int inPosAttribLoc;

	int invProjMatUnif;
	GBufferUniformLocations gtexUnif;
	PointLightUniformLocations unif;

};

class DirLightShader : public Shader
{
private:
	static const char* fileName;	// piece of code to be inserted in post_pass.frag

public:

	DirLightShader();

	int getInPosAttribLocation()const { return inPosAttribLoc; }

	int getInvProjMatUniform()const { return invProjMatUnif; }
	const DirLightUniformLocations& getUniformLocations()const;
	const GBufferUniformLocations& getGBufferUniformLocations()const { return gtexUnif; }

private:

	int inPosAttribLoc;

	int invProjMatUnif;
	GBufferUniformLocations gtexUnif;
	DirLightUniformLocations unif;

};

class SpotLightShader : public Shader
{
private:
	static const char* fileName;	// piece of code to be inserted in post_pass.frag

public:

	SpotLightShader();

	int getInPosAttribLocation()const { return inPosAttribLoc; }

	int getInvProjMatUniform()const { return invProjMatUnif; }
	const SpotLightUniformLocations& getUniformLocations()const;
	const GBufferUniformLocations& getGBufferUniformLocations()const { return gtexUnif; }

private:

	int inPosAttribLoc;

	int invProjMatUnif;
	GBufferUniformLocations gtexUnif;
	SpotLightUniformLocations unif;
};

#endif