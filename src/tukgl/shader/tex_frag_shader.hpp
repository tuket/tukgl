#ifndef TEX_FRAG_SHADER_HPP
#define TEX_FRAG_SHADER_HPP

#include "shader.hpp"

class RenderTarget;

class TexFragShader : public Shader
{

public:

	TexFragShader(const char* fragShadFile);

	virtual void use()const;

	int getUnifLoc(const char* name);

};

#endif