#ifndef SHADING_MODELS_HPP
#define SHADING_MODELS_HPP

#include <string>
#include <map>

enum class ShadingModelId
{
	DEFAULT = 0,
	PHONG = 0,
	UNLIT,
	CARTOON,

	NUM
};

static std::map<std::string, ShadingModelId>  shadingModelNameToId=
{
	{ "default", ShadingModelId::DEFAULT },
	{ "phong", ShadingModelId::PHONG },
	{ "unlit", ShadingModelId::UNLIT },
	{ "cartoon", ShadingModelId::CARTOON },
};

#endif