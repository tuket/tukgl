#ifndef SHADER_HPP
#define SHADER_HPP

#include <map>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class Shader
{
protected:
	Shader() {}

	bool compile(const char* vertShad, const char* fragShad);
	bool compileFromSrc(const char* vsSrc, const char* fsSrc);

	bool compile(const char* vertShad, const char* geomShad, const char* fragShad);
	bool compileFromSrc(const char* vsSrc, const char* gsSrc, const char* fsSrc);

	bool link();

public:

	virtual void use()const;
	unsigned getProgram()const { return program; }

	enum class UnifType
	{
		FLOAT,
		VEC2, VEC3, VEC4,
		INT, UNSIGNED,

		OTHER
	};

	union UnifData
	{
		float FLOAT;
		float VEC[4];
		int INT;
		unsigned UNSIGNED;
	};

	int getUnifLoc(const char* varName)const;

	UnifType getUnifType(int loc)const;
	UnifType getUnifType(const char* varName)const;

	void uploadUnifVal(int loc, const UnifData& data)const;
	void uploadUnifVal(int loc, float x)const;
	void uploadUnifVal(int loc, glm::vec2 v)const;
	void uploadUnifVal(int loc, glm::vec3 v)const;
	void uploadUnifVal(int loc, glm::vec4 v)const;
	void uploadUnifVal(int loc, int x)const;
	void uploadUnifVal(int loc, unsigned x)const;

	template<typename T>
	void uploadUnifVal(const char* varName, T t)
	{
		auto it = unifNameToLoc.find(varName);
		assert(it != unifNameToLoc.end());
		uploadUnifVal(it->second, t);
	}

protected:

	unsigned vshader;
	unsigned gshader; bool hasGeomShader;
	unsigned fshader;
	unsigned program;

	std::map<std::string, int> unifNameToLoc;
	std::map<int, UnifType> unifType;

protected:

	static const char* simpleQuadVSFileName;

	static UnifType fromGlUnifType(unsigned glUnifType);

};

class PostProcessShader : public Shader
{
	static const char* colorTexStr;
	static const char* depthTexStr;

public:
	
	PostProcessShader(const char* frag);

	void setInputTexture(unsigned tex);
	void setInputDepthTexture(unsigned tex);
	void setOutputFBO(unsigned FBO);

	virtual void use()const;

private:

	int texLoc;
	int depthTexLoc;
	unsigned texId;
	unsigned depthTexId;

	unsigned outFBO;

};

class PrePassShader : public Shader
{

public:
	enum TexSlot
	{
		COLOR = 0,
		SPECULAR,
		EMI,
		NORMAL,
		CUSTOM,

		NUM
	};

	enum class EAttribLocations : int
	{
		pos = 0,
		color,
		normal,
		texCoord,
		tangent
	};

	struct AttribLocations
	{
		int pos;
		int color;
		int normal;
		int texCoord;
		int tangent;
	};

	struct UniformLocations
	{
		// textures
		int colorTex;
		int specularTex;
		int normalTex;
		int emiTex;
		int customTex;

		// matrices
		int model;
		int modelView;
		int modelViewProj;
		int normal;

		// camera position
		int camPos;

		// shading model id
		int shadingModelId;

		// gloss
		int gloss;
	};

	PrePassShader(const char* vert, const char* frag);
	PrePassShader(const char* vert, const char* geom, const char* frag);
	void initPrePass();

	const AttribLocations& getAttribLocations()const { return attribLocs; }
	const UniformLocations& getUniformLocations()const { return unifLocs; }

private:
	
	AttribLocations attribLocs;
	UniformLocations unifLocs;

};

class SkyboxShader : public Shader
{

private:
	
	static const char* vertShadFileName;
	static const char* fragShadFileName;

public:

	enum class EAattribLocations { pos };
	struct UniformLocations
	{
		int colorTex;
		int viewProj;	//< this should be viewProj without translation
	};

public:
	SkyboxShader();

	int getPosAttribLocation()const { return posAttribLoc; }

	const UniformLocations& getUniformLocations()const { return unifLocs; }

private:

	int posAttribLoc;
	UniformLocations unifLocs;

};

#endif
