#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "../scene/scene.hpp"
#include "../shader/shader.hpp"
#include <vector>
#include "../shader/light_shaders.hpp"
#include "../util/convolution_matrix.hpp"
#include "../shader/tex_frag_shader.hpp"

class Renderer
{

public:

	Renderer();

	void render(Scene* scene, unsigned targetFbo=0);
	void renderToCustomTarget(Scene* scene, RenderTarget& renderTarget);

	void renderFragShader(TexFragShader& shader, RenderTarget& target);

	void enableMotionBlur(bool yes) { motionBlurEnabled = yes; }
	float getMotionBlurStrength()const { return motionBlurStrength; }
	void setMotionBlurStrength(float motionBlurStrength) { this->motionBlurStrength = motionBlurStrength; }

	void enableDof(bool yes) { dofEnabled = true; }
	float getDofFocalDistance()const { return dofFocalDist; }
	void setDofFocalDistance(float dist) { dofFocalDist = dist; }
	float getDofStrength()const { return dofMaxDistanceFactor; }
	void setDofStrength(float strength) { dofMaxDistanceFactor = strength; }

	bool isWireframeMode()const;
	void setWireframeMode(bool yes);

	void setConvolutionMatrix(const ConvolutionMatrix* maxtrix);

	void resize(unsigned w, unsigned h);

	unsigned getScreenWidth()const { return screenWidth; }
	unsigned getScreenHeight()const { return screenHeight; }

private:
	
	unsigned screenWidth, screenHeight;

	// wireframe mode
	unsigned polygonMode;

	// DOF
	bool dofEnabled;
	float dofFocalDist;
	float dofMaxDistanceFactor;

	// motion blur
	bool motionBlurEnabled;
	float motionBlurStrength;

	// convolution matrix
	const ConvolutionMatrix* convolutionMatrix;

	// TODO: add convolution masks

	// G-BUFFER
	unsigned gbuffer;
	unsigned gtex_albedoAndGloss;
	unsigned gtex_specular;
	unsigned gtex_emi;
	unsigned gtex_normal;
	unsigned gtex_depth;

	// light shaders
	PointLightShader plShader;
	DirLightShader dlShader;
	SpotLightShader slShader;

	// plane
	unsigned planeVAO;
	unsigned planeVBO;

	// FBOs
	static const unsigned NUM_FBO = 3;
	unsigned fbo[NUM_FBO];
	unsigned fboColorTex[NUM_FBO];	// TOOPT:
	unsigned fboDepthTex[NUM_FBO];
	unsigned skyboxFbo;

	// post process shaders
	PostProcessShader ppVoidShader;
	PostProcessShader ppCustomMaskShader;
	int uMask;
	PostProcessShader ppDofShader;
	int uFocalDistance;
	int uMaxDistanceFactor;
	int uNear, uFar;
	
	// skybox
	SkyboxShader skyboxShader;
	unsigned skyboxVao;

	// black texture
	unsigned blackTex;
	
private:

	void uploadGBufferUniforms(const GBufferUniformLocations& gtexUnif);

	void resizeFBO(unsigned w, unsigned h);

};

#endif