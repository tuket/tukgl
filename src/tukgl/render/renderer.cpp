#include "renderer.hpp"

#include <glad/glad.h>
#include "../mesh/mesh_pool.hpp"
#include "../shader/shader_pool.hpp"
#include <iostream>
#include "../mesh/plane.hpp"
#include "../scene/skybox.hpp"
#include "../texture/texture_pool.hpp"
#include "../texture/render_target.hpp"
#include "../shader/shader_pool.hpp"

//save de texturas
//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#include "stb/stb_image_write.h"

using namespace std;
using namespace glm;

Renderer::Renderer()
	:ppVoidShader("shaders/pp_void.frag"),
	ppCustomMaskShader("shaders/pp_mask.frag"),
	ppDofShader("shaders/pp_dof.frag")
{
	uMask = glGetUniformLocation(ppCustomMaskShader.getProgram(), "mask");
	uFocalDistance = glGetUniformLocation(ppDofShader.getProgram(), "focalDistance");
	uMaxDistanceFactor = glGetUniformLocation(ppDofShader.getProgram(), "maxDistanceFactor");
	uNear = glGetUniformLocation(ppDofShader.getProgram(), "near");
	uFar = glGetUniformLocation(ppDofShader.getProgram(), "far");

	polygonMode = GL_FILL;

	dofEnabled = false;
	dofFocalDist = 1;
	dofMaxDistanceFactor = 1;

	motionBlurEnabled = false;
	motionBlurStrength = 0.1f;

	convolutionMatrix = &ConvolutionMatrices::nothing;

	glGenFramebuffers(1, &gbuffer);
	glGenTextures(1, &gtex_albedoAndGloss);
	glGenTextures(1, &gtex_specular);
	glGenTextures(1, &gtex_emi);
	glGenTextures(1, &gtex_normal);
	glGenTextures(1, &gtex_depth);

	glGenFramebuffers(NUM_FBO, fbo);
	glGenTextures(NUM_FBO, fboColorTex);
	glGenTextures(NUM_FBO, fboDepthTex);

	glGenFramebuffers(1, &skyboxFbo);

	// init plane
	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);
	glGenBuffers(1, &planeVBO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex * sizeof(float) * 3,
		planeVertexPos, GL_STATIC_DRAW);
	glVertexAttribPointer(plShader.getInPosAttribLocation(), 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(plShader.getInPosAttribLocation());

	// init skybox vao
	skyboxVao = instanceSkyboxVao();

	// black tex
	TexturePool& texPool = TexturePool::getInstance();
	blackTex = texPool.getTexture("img/black.png");

}

void Renderer::renderFragShader(TexFragShader& shader, RenderTarget& target)
{
	shader.use();
	target.bind();

	glBindVertexArray(planeVAO);
	glViewport(0, 0, target.getWidth(), target.getHeight());
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void Renderer::render(Scene* scene, unsigned outputFbo)
{

	GOCamera* camera = scene->getActiveCamera();
	if (camera == nullptr)
	{
		cerr << "Error: no active camera" << endl;
		return;
	}
	camera->setAspectRatio((float)screenWidth/screenHeight);
	mat4 view = camera->getViewMat();
	mat4 proj = camera->getProjMat();
	mat4 viewProj = proj * view;
	mat4 invProjMat = inverse(proj);

	// -----------
	// PRE PASS
	// -----------

	// skybox
	glBindFramebuffer(GL_FRAMEBUFFER, skyboxFbo);

	glDisable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	if (scene->isSkyboxEnabled())
	{
		mat4 skyboxView = inverse(mat4_cast(camera->getSceneNode()->getRotation()));
		mat4 skyboxViewProj = proj * skyboxView;
		int posAttribLoc = skyboxShader.getPosAttribLocation();
		const SkyboxShader::UniformLocations& unifLocs = skyboxShader.getUniformLocations();
		unsigned texId = scene->getSkyboxCubeMap();

		skyboxShader.use();

		if (unifLocs.colorTex != -1)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_CUBE_MAP, texId);
			glUniform1i(unifLocs.colorTex, 0);
		}
		if (unifLocs.viewProj != -1)
		{
			glUniformMatrix4fv(unifLocs.viewProj, 1, GL_FALSE, &skyboxViewProj[0][0]);
		}

		glDepthMask(GL_FALSE);
		glBindVertexArray(skyboxVao);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthMask(GL_TRUE);
	}

	// meshes
	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glClear(GL_DEPTH_BUFFER_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

	for (GOMesh* go : scene->getMeshes())
	if(go->isEnabled())
	{
		MeshPool& meshPool = MeshPool::getInstance();
		ShaderPool& shaderPool = ShaderPool::getInstance();

		MeshId meshId = go->getMeshId();
		MeshVAO vao = meshPool.getMeshVAO(meshId);
		const Material& material = go->getMaterial();
		PrepassShaderId shaderId = material.getShaderId();
		const PrePassShader* shaderProg = shaderPool.getPrePassShader(shaderId);

		const PrePassShader::AttribLocations& attribLocs = shaderProg->getAttribLocations();
		const PrePassShader::UniformLocations& unifLocations = shaderProg->getUniformLocations();

		shaderProg->use();

		material.getFaceCulling() ?
			glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);

		// UPLOAD TEXTURES
		if (material.getUsesColorTexture() && unifLocations.colorTex != -1)
		{
			glActiveTexture(GL_TEXTURE0 + PrePassShader::TexSlot::COLOR);
			glBindTexture(GL_TEXTURE_2D, material.getColorTexture());
			glUniform1i(unifLocations.colorTex, PrePassShader::TexSlot::COLOR);
		}
		if (material.getUsesSpecularTexture() && unifLocations.specularTex != -1)
		{
			glActiveTexture(GL_TEXTURE0 + PrePassShader::TexSlot::SPECULAR);
			glBindTexture(GL_TEXTURE_2D, material.getSpecularTexture());
			glUniform1i(unifLocations.specularTex, PrePassShader::TexSlot::SPECULAR);
		}
		if (material.getUsesNormalTexture() && unifLocations.normalTex != -1)
		{
			glActiveTexture(GL_TEXTURE0 + PrePassShader::TexSlot::NORMAL);
			glBindTexture(GL_TEXTURE_2D, material.getNormalTexture());
			glUniform1i(unifLocations.normalTex, PrePassShader::TexSlot::NORMAL);
		}
		if (unifLocations.emiTex != -1)
		{
			glActiveTexture(GL_TEXTURE0 + PrePassShader::TexSlot::EMI);
			if (material.getUsesEmiTexture())
			{
				glBindTexture(GL_TEXTURE_2D, material.getEmiTexture());
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, blackTex);
			}
			glUniform1i(unifLocations.emiTex, PrePassShader::TexSlot::EMI);
		}
		if (material.getUsesCustomTexture() && unifLocations.customTex != -1)
		{
			glActiveTexture(GL_TEXTURE0 + PrePassShader::TexSlot::CUSTOM);
			glBindTexture(GL_TEXTURE_2D, material.getCustomTexture());
			glUniform1i(unifLocations.customTex, PrePassShader::TexSlot::CUSTOM);
		}

		// UPLOAD MATRICES
		if (unifLocations.model)
		{
			mat4 model = go->getSceneNode()->getModelMat();
			glUniformMatrix4fv(unifLocations.model, 1, GL_FALSE, &model[0][0]);
		}
		if (unifLocations.modelView)
		{
			mat4 model = go->getSceneNode()->getModelMat();
			mat4 modelView = view * model;
			glUniformMatrix4fv(unifLocations.modelView, 1, GL_FALSE, &modelView[0][0]);
		}
		if (unifLocations.modelViewProj != -1)
		{
			mat4 model = go->getSceneNode()->getModelMat();
			mat4 modelViewProj = viewProj * model;
			glUniformMatrix4fv(unifLocations.modelViewProj, 1, GL_FALSE, &modelViewProj[0][0]);
		}
		if (unifLocations.normal != -1)
		{
			mat4 model = go->getSceneNode()->getModelMat();
			mat4 modelView = view * model;
			mat4 normal = inverse(transpose(modelView));
			glUniformMatrix4fv(unifLocations.normal, 1, GL_FALSE, &normal[0][0]);
		}

		// UPLOAD CAMERA POSITON
		if (unifLocations.camPos != -1)
		{
			vec3 camPos = camera->getSceneNode()->getWorldPosition();
			glUniform3fv(unifLocations.camPos, 1, &camPos[0]);
		}

		// UPLOAD SHADING MODEL ID
		if (unifLocations.shadingModelId != -1)
		{
			glUniform1ui(unifLocations.shadingModelId, (unsigned)material.getShadingModelId());
		}

		// ULOAD GLOSS
		if (unifLocations.gloss != -1)
		{
			glUniform1f(unifLocations.gloss, material.getGlossiness());
		}

		// UPLOAD CUSTOM UNIFORMS
		material.uploadCustomUnifs();

		glBindVertexArray(vao);
		glDrawElements(GL_TRIANGLES, meshPool.getMeshNumTriangles(meshId) * 3,
			GL_UNSIGNED_INT, (void*)0);
		
	}

	// -----------------
	// POST PASS
	// -----------------
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// copy emissive to output buffer
	ppVoidShader.setOutputFBO(fbo[0]);
	ppVoidShader.setInputTexture(gtex_emi);
	ppVoidShader.use();

	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// illuminate !
	glBindFramebuffer(GL_FRAMEBUFFER, fbo[0]);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);	// lights accumulate

	// point lights
	if (!scene->getPointLights().empty())
	{
		plShader.use();

		PointLightUniformLocations unif = plShader.getUniformLocations();
		GBufferUniformLocations gtexUnif = plShader.getGBufferUniformLocations();
		int invProjMatUnif = plShader.getInvProjMatUniform();

		uploadGBufferUniforms(gtexUnif);
		glUniformMatrix4fv(invProjMatUnif, 1, GL_FALSE, &invProjMat[0][0]);
		glBindVertexArray(planeVAO);

		for (GOPointLight* l : scene->getPointLights())
		if (l->isEnabled())
		{
			PointLight data;
			data.Ia = l->getAmbientIntensity();
			data.Il = l->getDiffuseAndSpecularIntensity();
			data.c0 = l->getC0();
			data.c1 = l->getC1();
			data.c2 = l->getC2();
			// transform position to camera coords
			vec3 pos = l->getSceneNode()->getWorldPosition();
			data.Pl = vec3(view * vec4(pos, 1));

			uploadPointLightUniforms(unif, data);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		}
	}

	// dir lights
	if(!scene->getDirLights().empty())
	{
		dlShader.use();

		DirLightUniformLocations unif = dlShader.getUniformLocations();
		GBufferUniformLocations gtexUnif = dlShader.getGBufferUniformLocations();
		int invProjMatUnif = dlShader.getInvProjMatUniform();

		uploadGBufferUniforms(gtexUnif);
		glUniformMatrix4fv(invProjMatUnif, 1, GL_FALSE, &invProjMat[0][0]);
		glBindVertexArray(planeVAO);

		for (GODirLight* l : scene->getDirLights())
		if (l->isEnabled())
		{
			DirLight data;
			data.Ia = l->getAmbientIntensity();
			data.Il = l->getDiffuseAndSpecularIntensity();
			// transform direction to camera coords
			quat qrot = l->getSceneNode()->getRotation();
			vec3 dir = l->getDirection();
			dir = vec3(qrot * dir * inverse(qrot));
			dir = vec3(view * vec4(dir, 0));
			data.dir = dir;

			uploadDirLightUniforms(unif, data);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

	// spot lights
	if(!scene->getSpotLights().empty())
	{
		slShader.use();

		SpotLightUniformLocations unif = slShader.getUniformLocations();
		GBufferUniformLocations gtexUnif = slShader.getGBufferUniformLocations();
		int invProjMatUnif = slShader.getInvProjMatUniform();

		uploadGBufferUniforms(gtexUnif);
		glUniformMatrix4fv(invProjMatUnif, 1, GL_FALSE, &invProjMat[0][0]);
		glBindVertexArray(planeVAO);

		for (GOSpotLight* l : scene->getSpotLights())
		if(l->isEnabled())
		{
			SpotLight data;
			data.Ia = l->getAmbientIntensity();
			data.Il = l->getDiffuseAndSpecularIntensity();
			data.c0 = l->getC0();
			data.c1 = l->getC1();
			data.c2 = l->getC2();
			data.El = l->getHalfConeAperture();
			data.Ed = l->getAttenuationByAngle();
			// transform position to camera coords
			vec3 pos = l->getSceneNode()->getWorldPosition();
			data.Pl = vec3(view * vec4(pos, 1));
			// transform direction to camera coords
			quat qrot = l->getSceneNode()->getWorldRotation();
			quat qdir = (qrot * quat(0, 0, 0, -1) * inverse(qrot));
			vec3 dir = vec3(qdir.x, qdir.y, qdir.z);
			dir = vec3(inverse(transpose(view)) * vec4(dir, 0));
			data.dir = dir;

			uploadSpotLightUniforms(unif, data);

			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
	}

	//----------------
	// POST PROCESSES
	//----------------

	// -- custom mask filtering --
	ppCustomMaskShader.setOutputFBO(fbo[1]);
	ppCustomMaskShader.setInputTexture(fboColorTex[0]);
	ppCustomMaskShader.use();

	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	if (uMask != -1)
	{
		uploadMaskUniform(uMask, *convolutionMatrix);
	}
	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	unsigned nextInput = 1;
	unsigned nextOutput = 0;

	// -- depth of field --
	if (dofEnabled)
	{
		ppDofShader.setOutputFBO(fbo[nextOutput]);
		ppDofShader.setInputTexture(fboColorTex[nextInput]);
		ppDofShader.setInputDepthTexture(gtex_depth);
		ppDofShader.use();

		glClear(GL_COLOR_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glDisable(GL_BLEND);

		if (uFocalDistance != -1)
			glUniform1f(uFocalDistance, dofFocalDist);
		if (uMaxDistanceFactor != -1)
			glUniform1f(uMaxDistanceFactor, dofMaxDistanceFactor);
		if (uNear != -1)
			glUniform1f(uNear, camera->getNear());
		if (uFar != -1)
			glUniform1f(uFar, camera->getFar());

		glBindVertexArray(planeVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		std::swap(nextInput, nextOutput);
	}

	// -- motion blur --
	if (motionBlurEnabled)
	{
		ppVoidShader.setOutputFBO(fbo[2]);
		ppVoidShader.setInputTexture(fboColorTex[nextInput]);
		ppVoidShader.use();

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);

		glBlendFunc(GL_CONSTANT_COLOR, GL_CONSTANT_ALPHA);
		glBlendColor(1 - motionBlurStrength, 1 - motionBlurStrength, 1 - motionBlurStrength, motionBlurStrength);
		glBlendEquation(GL_FUNC_ADD);

		glBindVertexArray(planeVAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		nextInput = 2;
	}

	// -- move to back buffer --
	ppVoidShader.setOutputFBO(outputFbo);
	ppVoidShader.setInputTexture(fboColorTex[nextInput]);
	ppVoidShader.use();

	glClear(GL_COLOR_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

}

void Renderer::renderToCustomTarget(Scene* scene, RenderTarget& renderTarget)
{
	int prevW = screenWidth, prevH = screenHeight;
	resize(renderTarget.getWidth(), renderTarget.getHeight());
	render(scene, renderTarget.getFbo());
	resize(prevW, prevH);
}

bool Renderer::isWireframeMode()const
{
	return polygonMode == GL_LINE;
}

void Renderer::setWireframeMode(bool yes)
{
	polygonMode = (yes ? GL_LINE : GL_FILL);
}

void Renderer::resize(unsigned w, unsigned h)
{
	screenWidth = w;
	screenHeight = h;

	glViewport(0, 0, w, h);

	resizeFBO(w, h);
}

// private

void Renderer::uploadGBufferUniforms(const GBufferUniformLocations& gtexUnif)
{

	auto upload = [](const unsigned texSlot, const int loc, const unsigned texId)
	{
		glActiveTexture(GL_TEXTURE0 + texSlot);
		glBindTexture(GL_TEXTURE_2D, texId);
		glUniform1i(loc, texSlot);
	};
	
	{
		const unsigned texSlot = 0;
		const int loc = gtexUnif.albedoAndGloss;
		const unsigned texId = gtex_albedoAndGloss;
		upload(texSlot, loc, texId);
	}
	{
		const unsigned texSlot = 1;
		const int loc = gtexUnif.specularAndShadingModelId;
		const unsigned texId = gtex_specular;
		upload(texSlot, loc, texId);
	}
	{
		const unsigned texSlot = 2;
		const int loc = gtexUnif.emi;
		const unsigned texId = gtex_emi;
		upload(texSlot, loc, texId);
	}
	{
		const unsigned texSlot = 3;
		const int loc = gtexUnif.normal;
		const unsigned texId = gtex_normal;
		upload(texSlot, loc, texId);
	}
	{
		const unsigned texSlot = 4;
		const int loc = gtexUnif.depth;
		const unsigned texId = gtex_depth;
		upload(texSlot, loc, texId);
	}
}

void Renderer::resizeFBO(unsigned w, unsigned h)
{

	// G-Buffer
	glBindTexture(GL_TEXTURE_2D, gtex_albedoAndGloss);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, gtex_specular);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, gtex_emi);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, gtex_normal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
		GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, gtex_depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
		GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_2D, gtex_albedoAndGloss, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1,
		GL_TEXTURE_2D, gtex_specular, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2,
		GL_TEXTURE_2D, gtex_emi, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3,
		GL_TEXTURE_2D, gtex_normal, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
		GL_TEXTURE_2D, gtex_depth, 0);

	const GLenum buffs[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
	glDrawBuffers(4, buffs);
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		std::cerr << "Error configuring the FBO (G-Buffer)" << std::endl;
		exit(-1);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// Other FBOs

	// color buffer
	for (unsigned i = 0; i < NUM_FBO; i++)
	{
		glBindTexture(GL_TEXTURE_2D, fboColorTex[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,
			GL_RGBA, GL_FLOAT, NULL);	// mutable
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	// depth buffer
	for (unsigned i = 0; i < NUM_FBO; i++)
	{
		glBindTexture(GL_TEXTURE_2D, fboDepthTex[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	for (unsigned i = 0; i < NUM_FBO; i++)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, fbo[i]);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_2D, fboColorTex[i], 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
			fboDepthTex[i], 0);

		const GLenum buffs[1] = { GL_COLOR_ATTACHMENT0 };	// layaout 0 -> color attachment 0
		glDrawBuffers(1, buffs);
		if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
		{
			std::cerr << "Error configurando el FBO" << std::endl;
			exit(-1);
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	// skybox fbo
	{
		glBindFramebuffer(GL_FRAMEBUFFER, skyboxFbo);

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
			GL_TEXTURE_2D, gtex_emi, 0);

		const GLenum buffs[1] = { GL_COLOR_ATTACHMENT0 };	// layaout 0 -> color attachment 0
		glDrawBuffers(1, buffs);
		if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
		{
			std::cerr << "Error configurando el FBO" << std::endl;
			exit(-1);
		}
		glClear(GL_COLOR_BUFFER_BIT);
	}

}

void Renderer::setConvolutionMatrix(const ConvolutionMatrix* matrix)
{
	convolutionMatrix = matrix;
}