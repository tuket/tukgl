#ifndef LIGHTS_HPP
#define LIGHTS_HPP

#include <string>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>

class Camera;

// POINT LIGHT
struct PointLight
{
	glm::vec3 Pl;	// position of the light in world coords
	glm::vec3 Ia;	// ambient intensity
	glm::vec3 Il;	// diffuse and specular intensity
	float c0;		// attenuation by distance (constant)
	float c1;		// attenuation by distance (linear)
	float c2;		// attenuation by distance (quadratic)
};
struct PointLightUniformLocations
{
	int Pl, Ia, Il;
	int c0, c1, c2;
};
// get point light uniform locations
void getPointLightUniformLocations
(
	PointLightUniformLocations& unif,
	std::string plName,
	unsigned program
);
// uploads the uniform data (the shader program must be bound)
void uploadPointLightUniforms
(
	const PointLightUniformLocations& unif,
	const PointLight& pl
);

// DIR LIGHT
struct DirLight
{
	glm::vec3 dir;		// direction of the light in world coords
	glm::vec3 Ia;		// ambient intensity
	glm::vec3 Il;		// diffuse and specular intensity
};
struct DirLightUniformLocations
{
	int dir, Ia, Il;
};
// get point light uniform locations
void getDirLightUniformLocations
(
	DirLightUniformLocations& unif,
	std::string dlName,
	unsigned program
);
// uploads the uniform data (the shader program must be bound)
void uploadDirLightUniforms
(
	const DirLightUniformLocations& unif,
	const DirLight& dl
);

// SPOT LIGHT
struct SpotLight
{
	glm::vec3 Pl;		// light position in world coords
	glm::vec3 dir;		// direction in world coords
	glm::vec3 Ia;		// ambient intensity
	glm::vec3 Il;		// diffuse and specular intensity
	float c0;			// attenuation by distance (constant)
	float c1;			// attenuation by distance (linear)
	float c2;			// attenuation by distance (quadratic)
	float El;			// cone angle aperture (half)
	float Ed;			// attenuation by angle ([1, +inf], 1->no attenuation)
};
struct SpotLightUniformLocations
{
	int Pl, dir;
	int Ia, Il;
	int c0, c1, c2;
	int El, Ed;
};
// get spot light uniform locations
void getSpotLightUniformLocations
(
	SpotLightUniformLocations& unif,
	std::string plName,
	unsigned program
);
// uploads the uniform data (the shader program must be bound)
void uploadSpotLightUniforms
(
	const SpotLightUniformLocations& unif,
	const SpotLight& sl
);

#endif