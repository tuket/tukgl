#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "../shader/shading_models.hpp"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include "../shader/shader.hpp"
#include "../shader/shader_pool.hpp"

typedef unsigned PrepassShaderId;
typedef int TextureId;

namespace pugi
{
	class xml_document;
};

class Material
{
public:

	Material(PrepassShaderId shaderId = -1) :
		shaderId(shaderId),
		shadingModelId(ShadingModelId::DEFAULT),
		faceCulling(true)
	{}

	PrepassShaderId getShaderId()const;
	void setShaderID(PrepassShaderId shaderId);

	void setColorTextureId(TextureId texId);
	void setSpecularTextureId(TextureId texId);
	void setNormalTextureId(TextureId texId);
	void setEmissiveTextureId(TextureId texId);
	void setCustomTextureId(TextureId texId);

	void setColorTexture(const char* fileName);
	void setSpecularTexture(const char* fileName);
	void setNormalTexture(const char* fileName);
	void setEmissiveTexture(const char* fileName);
	void setCustomTexture(const char* fileName);

	bool getUsesColorTexture()const;
	bool getUsesSpecularTexture()const;
	bool getUsesNormalTexture()const;
	bool getUsesEmiTexture()const;
	bool getUsesCustomTexture()const;

	TextureId getColorTexture()const { return colorTex; }
	TextureId getSpecularTexture()const { return specularTex; }
	TextureId getNormalTexture()const { return normalTex; }
	TextureId getEmiTexture()const { return emiTex; }
	TextureId getCustomTexture()const { return customTex; }

	void setUsesColorTexture(bool yes);
	void setUsesSpecularTexture(bool yes);
	void setUsesNormalTexture(bool yes);
	void setUsesEmissiveTexture(bool yes);
	void setUsesCustomTexture(bool yes);

	float getGlossiness()const { return glossiness; }
	void setGlossiness(float glossiness);

	bool getFaceCulling()const { return faceCulling; }
	void setFaceCulling(bool yes) { faceCulling = yes; }

	void loadMaterialFromFile(const char* fileName);
	void loadMaterialFromStr(const char* str);
	void loadMaterialFromXmlDoc(const pugi::xml_document& doc);

	ShadingModelId getShadingModelId()const { return shadingModelId; }
	void setShadingModelId(ShadingModelId id) { shadingModelId = id; }

	void uploadCustomUnifs()const;

	bool setUnifValue(int loc, float x);
	bool setUnifValue(int loc, glm::vec2 v);
	bool setUnifValue(int loc, glm::vec3 v);
	bool setUnifValue(int loc, glm::vec4 v);
	bool setUnifValue(int loc, int x);
	bool setUnifValue(int loc, unsigned x);

	template <typename T>
	bool setUnifValue(const char* varName, T t)
	{
		ShaderPool& shaderPool = ShaderPool::getInstance();
		const PrePassShader* shader = shaderPool.getPrePassShader(getShaderId());
		int unifLoc = shader->getUnifLoc(varName);
		assert(unifLoc >= 0);
		return setUnifValue(unifLoc, t);
	}

private:

	PrepassShaderId shaderId;
	ShadingModelId shadingModelId;

	// negative values mean not used
	TextureId colorTex = -1;
	TextureId specularTex = -1;
	TextureId normalTex = -1;
	TextureId emiTex = -1;
	TextureId customTex = -1;

	float glossiness;

	bool faceCulling;

	std::map<int, Shader::UnifData> unifValue;	//< uniform location to value (prepass)

public:
	// static funcitons
	static Material createBasicUnlit();
	static Material createBasicPhong();

};

#endif
