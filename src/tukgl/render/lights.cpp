#include "lights.hpp"

#include <glad/glad.h>

using namespace glm;

// POINT LIGHT
void getPointLightUniformLocations
(
	PointLightUniformLocations& unif,
	std::string plName,
	unsigned program
)
{
	std::string unifName;

	unifName = plName + ".Pl";
	unif.Pl = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c0";
	unif.c0 = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c1";
	unif.c1 = glGetUniformLocation(program, unifName.c_str());

	unifName = plName + ".c2";
	unif.c2 = glGetUniformLocation(program, unifName.c_str());
}

void uploadPointLightUniforms
(
	const PointLightUniformLocations& unif,
	const PointLight& pl
)
{
	if (unif.Pl != -1) glUniform3fv(unif.Pl, 1, &pl.Pl[0]);
	if (unif.Ia != -1) glUniform3fv(unif.Ia, 1, &pl.Ia[0]);
	if (unif.Il != -1) glUniform3fv(unif.Il, 1, &pl.Il[0]);
	if (unif.c0 != -1) glUniform1f(unif.c0, pl.c0);
	if (unif.c1 != -1) glUniform1f(unif.c1, pl.c1);
	if (unif.c2 != -1) glUniform1f(unif.c2, pl.c2);
}


// DIR LIGHT
void getDirLightUniformLocations
(
	DirLightUniformLocations& unif,
	std::string dlName,
	unsigned program
)
{
	std::string unifName;

	unifName = dlName + ".dir";
	unif.dir = glGetUniformLocation(program, unifName.c_str());

	unifName = dlName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = dlName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());
}

void uploadDirLightUniforms
(
	const DirLightUniformLocations& unif,
	const DirLight& dl
)
{
	if (unif.dir != -1) glUniform3fv(unif.dir, 1, &dl.dir[0]);
	if (unif.Ia != -1) glUniform3fv(unif.Ia, 1, &dl.Ia[0]);
	if (unif.Il != -1) glUniform3fv(unif.Il, 1, &dl.Il[0]);
}


// SPOT LIGHT
void getSpotLightUniformLocations
(
	SpotLightUniformLocations& unif,
	std::string slName,
	unsigned program
)
{
	std::string unifName;

	unifName = slName + ".Pl";
	unif.Pl = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".dir";
	unif.dir = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Ia";
	unif.Ia = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Il";
	unif.Il = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c0";
	unif.c0 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c1";
	unif.c1 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".c2";
	unif.c2 = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".El";
	unif.El = glGetUniformLocation(program, unifName.c_str());

	unifName = slName + ".Ed";
	unif.Ed = glGetUniformLocation(program, unifName.c_str());
}

void uploadSpotLightUniforms
(
	const SpotLightUniformLocations& unif,
	const SpotLight& sl
)
{

	if (unif.Pl != -1) glUniform3fv(unif.Pl, 1, &sl.Pl[0]);
	if (unif.dir != -1) glUniform3fv(unif.dir, 1, &sl.dir[0]);
	if (unif.Ia != -1) glUniform3fv(unif.Ia, 1, &sl.Ia[0]);
	if (unif.Il != -1) glUniform3fv(unif.Il, 1, &sl.Il[0]);
	if (unif.c0 != -1) glUniform1f(unif.c0, sl.c0);
	if (unif.c1 != -1) glUniform1f(unif.c1, sl.c1);
	if (unif.c2 != -1) glUniform1f(unif.c2, sl.c2);
	if (unif.El != -1) glUniform1f(unif.El, sl.El);
	if (unif.Ed != -1) glUniform1f(unif.Ed, sl.Ed);
}