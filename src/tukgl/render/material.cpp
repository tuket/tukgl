#include "material.hpp"

#include <cmath>
#include "pugixml.hpp"
#include <iostream>
#include "../shader/shader_pool.hpp"
#include "../texture/texture_pool.hpp"
#include "../shader/predefined_shaders.hpp"

PrepassShaderId Material::getShaderId()const
{
	return shaderId;
}

void Material::setShaderID(PrepassShaderId shaderId)
{
	this->shaderId = shaderId;
}

void Material::setColorTextureId(TextureId texId)
{
	colorTex = texId;
}

void Material::setSpecularTextureId(TextureId texId)
{
	specularTex = texId;
}

void Material::setNormalTextureId(TextureId texId)
{
	normalTex = texId;
}

void Material::setEmissiveTextureId(TextureId texId)
{
	emiTex = texId;
}

void Material::setCustomTextureId(TextureId texId)
{
	customTex = texId;
}

void Material::setColorTexture(const char* fileName)
{
	TexturePool& texturePool = TexturePool::getInstance();
	TextureId texId = texturePool.getTexture(fileName);
	setColorTextureId(texId);
}

void Material::setSpecularTexture(const char* fileName)
{
	TexturePool& texturePool = TexturePool::getInstance();
	TextureId texId = texturePool.getTexture(fileName);
	setSpecularTextureId(texId);
}

void Material::setNormalTexture(const char* fileName)
{
	TexturePool& texturePool = TexturePool::getInstance();
	TextureId texId = texturePool.getTexture(fileName);
	setNormalTextureId(texId);
}

void Material::setEmissiveTexture(const char* fileName)
{
	TexturePool& texturePool = TexturePool::getInstance();
	TextureId texId = texturePool.getTexture(fileName);
	setEmissiveTextureId(texId);
}
void Material::setCustomTexture(const char* fileName)
{
	TexturePool& texturePool = TexturePool::getInstance();
	TextureId texId = texturePool.getTexture(fileName);
	setCustomTextureId(texId);
}

bool Material::getUsesColorTexture()const
{
	return colorTex > 0;
}

bool Material::getUsesSpecularTexture()const
{
	return specularTex > 0;
}

bool Material::getUsesNormalTexture()const
{
	return normalTex > 0;
}

bool Material::getUsesEmiTexture()const
{
	return emiTex > 0;
}

bool Material::getUsesCustomTexture()const
{
	return customTex > 0;
}

void Material::setUsesColorTexture(bool yes)
{
	TextureId texIdAbs = abs(colorTex);
	if (yes) colorTex = texIdAbs;
	else	 colorTex = -texIdAbs;
}

void Material::setUsesSpecularTexture(bool yes)
{
	TextureId texIdAbs = abs(specularTex);
	if (yes) specularTex = texIdAbs;
	else	 specularTex = -texIdAbs;
}

void Material::setUsesNormalTexture(bool yes)
{
	TextureId texIdAbs = abs(normalTex);
	if (yes) normalTex = texIdAbs;
	else	 normalTex = -texIdAbs;
}

void Material::setUsesEmissiveTexture(bool yes)
{
	TextureId texIdAbs = abs(emiTex);
	if (yes) emiTex = texIdAbs;
	else	 emiTex = -texIdAbs;
}

void Material::setUsesCustomTexture(bool yes)
{
	TextureId texIdAbs = abs(customTex);
	if (yes) customTex = texIdAbs;
	else	 customTex = -texIdAbs;
}

void Material::setGlossiness(float glossiness)
{
	this->glossiness = glossiness;
}

void Material::loadMaterialFromFile(const char* fileName)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(fileName);
	if (result)
	{
		loadMaterialFromXmlDoc(doc);
	}
	else
	{
		std::cerr << "error loading material: " << fileName << std::endl;
	}
}

void Material::loadMaterialFromStr(const char* str)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_string(str);
	if (result)
	{
		loadMaterialFromXmlDoc(doc);
	}
	else
	{
		std::cerr << "error loading material from string: " << str << std::endl;
	}
}

void Material::loadMaterialFromXmlDoc(const pugi::xml_document& doc)
{
	TexturePool& texPool = TexturePool::getInstance();

	pugi::xml_node node_material = doc.child("material");
	pugi::xml_node node_textures = node_material.child("textures");
	pugi::xml_node node_color = node_textures.child("color");
	pugi::xml_node node_specular = node_textures.child("specular");
	pugi::xml_node node_normal = node_textures.child("normal");
	pugi::xml_node node_emi = node_textures.child("emi");
	pugi::xml_node node_custom = node_textures.child("custom");

	if (node_color)
	{
		const char* fn = node_color.text().as_string();
		setColorTexture(fn);
	}
	else setColorTextureId(-1);

	if (node_specular)
	{
		const char* fn = node_specular.text().as_string();
		setSpecularTexture(fn);
	}
	else setSpecularTextureId(-1);

	if (node_normal)
	{
		const char* fn = node_normal.text().as_string();
		setNormalTexture(fn);
	}
	else setNormalTextureId(-1);

	if (node_emi)
	{
		const char* fn = node_emi.text().as_string();
		setEmissiveTexture(fn);
	}
	else setEmissiveTextureId(-1);

	if (node_custom)
	{
		const char* fn = node_custom.text().as_string();
		setCustomTexture(fn);
	}
	else setCustomTextureId(-1);

	pugi::xml_node node_gloss = node_material.child("gloss");
	if (node_gloss)
	{
		glossiness = node_gloss.text().as_float();
	}
	else
	{
		glossiness = 0.f;
	}

	ShaderPool& shaderPool = ShaderPool::getInstance();
	pugi::xml_node node_prepass = node_material.child("prepass");
	if (node_prepass)
	{
		setShaderID(shaderPool.getPrePassShaderIdFromName(node_prepass.text().as_string()));
	}
	else
	{
		setShaderID(shaderPool.getDefaultPrePassShaderId());
	}

	pugi::xml_node node_shadingmodel = node_material.child("shadingmodel");
	if (node_shadingmodel)
	{
		shadingModelId = shadingModelNameToId[node_shadingmodel.text().as_string()];
	}
	else
	{
		shadingModelId = ShadingModelId::DEFAULT;
	}

}

void Material::uploadCustomUnifs()const
{
	ShaderPool& shaderPool = ShaderPool::getInstance();
	const PrePassShader* shader = shaderPool.getPrePassShader(getShaderId());
	for (auto p : unifValue)
	{
		int loc = p.first;
		Shader::UnifData data = p.second;
		shader->uploadUnifVal(loc, data);
	}
}

bool Material::setUnifValue(int loc, float x)
{
	Shader::UnifData data;
	data.FLOAT = x;
	unifValue[loc] = data;
	return true;
}
bool Material::setUnifValue(int loc, glm::vec2 v)
{
	Shader::UnifData data;
	data.VEC[0] = v.x;
	data.VEC[1] = v.y;
	unifValue[loc] = data;
	return true;
}
bool Material::setUnifValue(int loc, glm::vec3 v)
{
	Shader::UnifData data;
	data.VEC[0] = v.x;
	data.VEC[1] = v.y;
	data.VEC[2] = v.z;
	unifValue[loc] = data;
	return true;
}
bool Material::setUnifValue(int loc, glm::vec4 v)
{
	Shader::UnifData data;
	data.VEC[0] = v.x;
	data.VEC[1] = v.y;
	data.VEC[2] = v.z;
	data.VEC[3] = v.w;
	unifValue[loc] = data;
	return true;
}
bool Material::setUnifValue(int loc, int x)
{
	Shader::UnifData data;
	data.INT = x;
	unifValue[loc] = data;
	return true;
}
bool Material::setUnifValue(int loc, unsigned x)
{
	Shader::UnifData data;
	data.UNSIGNED = x;
	unifValue[loc] = data;
	return true;
}


// STATIC FUNCTIONS

Material Material::createBasicUnlit()
{
	Material mat;
	const ShaderPool& shaderPool = ShaderPool::getInstance();
	PrepassShaderId id = shaderPool.getPrePassShaderIdFromName(PreDefShad::simpleUnlitColor);
	mat.setShaderID(id);
	mat.setShadingModelId(ShadingModelId::UNLIT);
	mat.setUnifValue("baseColor", glm::vec3(0.8, 0.8, 0.8));
	return mat;
}

/*Material Material::createBasicPhong()
{
	// TODO
}*/