#ifndef SPLINES_HPP
#define SPLINES_HPP

#include <glm/vec2.hpp>
#include <vector>

void generateRandCircularControlPoints(std::vector<glm::vec2>& output,
	unsigned numPoints, float scale,
	float jit = 0.3f);


class Catmull
{

	const unsigned NUM_SAMPLES = 100;

public:

	Catmull() {}
	Catmull(const std::vector<glm::vec2>& points, float tension);

	float get(float t, glm::vec2& position, glm::vec2& forward = _dummy_vector_)const;
	float curveLength()const;

private:

	std::vector<glm::vec2> points;
	float tau;	// tension

	std::vector<float> lengths;
	std::vector<glm::vec2> positions;
	std::vector<glm::vec2> forwards;

	void computeLengths();

	int index(int i)const;

	int binarySearch(float t)const;

	static glm::vec2 interpolate(float A, float b, float C, glm::vec2 x1, glm::vec2 x2);

private:
	static glm::vec2 _dummy_vector_;

};

#endif