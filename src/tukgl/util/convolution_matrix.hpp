#ifndef CONVOLUTION_MATRIX_HPP
#define CONVOLUTION_MATRIX_HPP

const unsigned MASK_SIZE = 25;

typedef float ConvolutionMatrix[MASK_SIZE];

// the shader program must be bound
void uploadMaskUniform(int uMask, const ConvolutionMatrix& mask);

namespace ConvolutionMatrices
{

	const ConvolutionMatrix nothing =
	{
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	};

	const ConvolutionMatrix sharpen =
	{
		0, 0, 0, 0, 0,
		0, 0, -1, 0, 0,
		0, -1, 5, -1, 0,
		0, 0, -1, 0, 0,
		0, 0, 0, 0, 0
	};

	const ConvolutionMatrix blur =
	{
		0, 0, 0, 0, 0,
		0, 1.f/9.f, 1.f / 9.f, 1.f / 9.f, 0,
		0, 1.f / 9.f, 1.f / 9.f, 1.f / 9.f, 0,
		0, 1.f / 9.f, 1.f / 9.f, 1.f / 9.f, 0,
		0, 0, 0, 0, 0
	};

	const ConvolutionMatrix edgeEnhance =
	{
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0, -1, 1, 0, 0,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0
	};

	const ConvolutionMatrix edgeDetect =
	{
		0, 0, 0, 0, 0,
		0, 0, -1, 0, 0,
		0, -1, 4, -1, 0,
		0, 0, -1, 0, 0,
		0, 0, 0, 0, 0
	};

	const ConvolutionMatrix emboss =
	{
		0, 0, 0, 0, 0,
		0, -2, -1, 0, 0,
		0, -1, 1, 1, 0,
		0, 0, 1, 2, 0,
		0, 0, 0, 0, 0
	};

}

#endif