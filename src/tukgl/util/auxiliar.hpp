#ifndef AUXILIAR_HPP
#define AUXILIAR_HPP

#include <glm/gtc/constants.hpp>
#include <string>

const float PI = glm::pi<float>();

static inline float toRad(float a)
{
	return a * (PI / 180.f);
}

static inline float toDeg(float a)
{
	return a * (180.f / PI);
}

//Caraga una textura y devuelve un puntero a su ubicacion en mememoria principal
//tambi�n devuelve el tama�o de la textura (w,h)
//!!Ya implementada
unsigned char *loadBitmap(const char* fileName, unsigned &w, unsigned &h);

//Carga un fichero en una cadena de caracteres
char *loadStringFromFile(const char *fileName, int &fileLen);

// normalize [0, 2*PI]
static inline float normalizeAngle2PI(float a)
{
	while (a < 0) a += 2 * PI;
	while (a > 2 * PI) a -= 2 * PI;
	return a;
}

// normalize [0, 2*PI]
static inline float normalizeAnglePI(float a)
{
	while (a < -PI) a += 2 * PI;
	while (a > +PI) a -= 2 * PI;
	return a;
}

static bool replaceStr(std::string& str, const std::string& from, const std::string& to) {
	size_t start_pos = str.find(from);
	if (start_pos == std::string::npos)
		return false;
	str.replace(start_pos, from.length(), to);
	return true;
}

#endif