#include "convolution_matrix.hpp"

#include <glad/glad.h>

void uploadMaskUniform(int uMask, const ConvolutionMatrix& mask)
{
	glUniform1fv(uMask, MASK_SIZE, mask);
}