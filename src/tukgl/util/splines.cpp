#include "splines.hpp"
#include <iostream>
#include <random>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/norm.hpp>
#include <vector>

using namespace std;
using namespace glm;

const float PI = pi<float>();

void generateRandCircularControlPoints(
	vector<vec2>& output,
	unsigned numPoints, float scale,
	float jit)
{

	if (numPoints < 4)
	{
		cerr << "Error number of control points must be greater than 3" << endl;
	}

	const float r = scale;
	unsigned n = numPoints;

	random_device rd;
	default_random_engine engine(rd());
	uniform_real_distribution<float> unifDist(r * (1.f - jit), r * (1.f + jit));

	output = vector<vec2>(numPoints);

	for (int i = 0; i < n; i++)
	{

		float angle = ((float)i / n) * (2 * PI);
		float d = unifDist(engine);

		vec2 p;
		p.x = cos(angle) * d;
		p.y = sin(angle) * d;
		output[i] = p;

	}

}

// -------------------------------------------------
// CATMULL ROM
// -------------------------------------------------

Catmull::Catmull(const vector<vec2>& points, float tension)
	:points(points), tau(tension)
{
	computeLengths();
}

void Catmull::computeLengths()
{

	const unsigned n = points.size();

	lengths = vector<float>(NUM_SAMPLES * n);
	positions = vector<vec2>(NUM_SAMPLES * n);
	forwards = vector<vec2>(NUM_SAMPLES * n);

	vec2 pp = points[0];

	for (int i = 0; i < n; i++)
	{
		vec2 P0 = points[index(i - 1)];
		vec2 P1 = points[index(i + 0)];
		vec2 P2 = points[index(i + 1)];
		vec2 P3 = points[index(i + 2)];

		vec2 c0 = P1;
		vec2 c1 = -tau * P0 + tau * P2;
		vec2 c2 = 2 * tau * P0 + (tau - 3) * P1 + (3 - 2 * tau) * P2 - tau * P3;
		vec2 c3 = -tau * P0 + (2 - tau) * P1 + (tau - 2) * P2 + tau * P3;

		for (int j = 0; j < NUM_SAMPLES; j++)
		{
			int ind = j + i * NUM_SAMPLES;
			float u = (float)j / NUM_SAMPLES;
			vec2 p = c0 + c1*u + c2*u*u + c3*u*u*u;
			//vec2 f = c1 + c2*u + c3*u*u;
			float len = distance(p, pp);
			positions[ind] = p;
			//forwards[ind] = f;
			lengths[ind] = len;
			pp = p;
		}

	}

	for (int i = 0; i < forwards.size(); i++)
	{
		forwards[i] =
			positions[(i+1) % forwards.size()] -
			positions[i];
	}

	for (int i = 1; i < lengths.size(); i++)
	{
		lengths[i] += lengths[i - 1];
	}
	for (int i = 0; i<lengths.size() - 1; i++)
	{
		lengths[i] = lengths[i + 1];
	}
	lengths.back() = lengths[lengths.size() - 2];
	lengths.back() += distance(pp, points[0]);

}

float Catmull::get(float t, vec2& position, vec2& forward)const
{
	const float cl = curveLength();
	while (t < 0) t += cl;
	while (t >= cl)	t -= cl;

	int i = binarySearch(t);
	int j = i + 1;
	if (j == lengths.size())
	{
		j = i;
		i--;
	}

	float len1 = lengths[i];
	float len2 = lengths[j];

	position = interpolate(len1, t, len2, positions[i], positions[j]);
	forward = interpolate(len1, t, len2, forwards[i], forwards[j]);

	return t;
}

float Catmull::curveLength()const
{
	return lengths.back();
}

int Catmull::index(int i)const
{
	i += points.size();
	i %= points.size();
	return i;
}

// asumes 0 <= t < curveLength
int Catmull::binarySearch(float t)const
{
	int i = 0;
	int j = lengths.size();

	while (i < j - 1)
	{
		int ij = (i + j) / 2;
		float l = lengths[ij];
		if (l < t) i = ij;
		else if (l > t) j = ij;
		else return ij;
	}

	return i;
}

vec2 Catmull::interpolate(float A, float b, float C, vec2 x1, vec2 x2)
{
	float perc = (b - A) / (C - A);
	return (1 - perc) * x1 + perc * x2;
}



//////
vec2 Catmull::_dummy_vector_;